# 键盘快捷键

[[toc]]

Tiptap 带有合理的键盘快捷键默认值。根据您想要使用它的用途，您可能需要根据自己的喜好更改这些键盘快捷键。让我们看看我们为您定义的内容，然后向您展示如何更改它！

## 预定义的键盘快捷键
大多数核心扩展都会注册自己的键盘快捷键。根据您使用的扩展集，并非下面列出的所有键盘快捷键都适用于您的编辑器。

### 常用
命令|视窗/Linux|苹果系统
---|---|---
复制|`Control`+`C`|`Cmd`+`C`
剪切|`Control`+`X`|`Cmd`+`X`
粘贴|`Control`+`V`|`Cmd`+`V`
不格式化直接粘贴|`Control`+`Shift`+`V`|`Cmd`+`Shift`+`V`
撤消|`Control`+`Z`|`Cmd`+`Z`
重做|`Control`+`Shift`+`Z`|`Cmd`+`Shift`+`Z`
添加换行符|`Shift`+`Enter`|`Shift`+`Enter`

### 文本格式
命令|视窗/Linux|苹果系统
---|---|---
加粗|`Control`+`B`|`Cmd`+`B`
斜体|`Control`+`I`|`Cmd`+`I`
下划线|`Control`+`U`|`Cmd`+`U`
删除线|`Control`+`Shift`+`X`|`Cmd`+`Shift`+`X`
强调|`Control`+`Shift`+`H`|`Cmd`+`Shift`+`H`
代码|`Control`+`E`|`Cmd`+`E`

### 段落格式
命令|视窗/Linux|苹果系统
---|---|---
应用普通文本样式|`Control`+`Alt`+`0`|`Cmd`+`Alt`+`0`
应用标题样式 1|`Control`+`Alt`+`1`|`Cmd`+`Alt`+`1`
应用标题样式 2|`Control`+`Alt`+`2`|`Cmd`+`Alt`+`2`
应用标题样式 3|`Control`+`Alt`+`3`|`Cmd`+`Alt`+`3`
应用标题样式 4|`Control`+`Alt`+`4`|`Cmd`+`Alt`+`4`
应用标题样式 5|`Control`+`Alt`+`5`|`Cmd`+`Alt`+`5`
应用标题样式 6|`Control`+`Alt`+`6`|`Cmd`+`Alt`+`6`
有序列表|`Control`+`Shift`+`7`|`Cmd`+`Shift`+`7`
项目符号列表|`Control`+`Shift`+`8`|`Cmd`+`Shift`+`8`
任务列表|`Control`+`Shift`+`9`|`Cmd`+`Shift`+`9`
块引用|`Control`+`Shift`+`B`|`Cmd`+`Shift`+`B`
左对齐|`Control`+`Shift`+`L`|`Cmd`+`Shift`+`L`
居中对齐|`Control`+`Shift`+`E`|`Cmd`+`Shift`+`E`
右对齐|`Control`+`Shift`+`R`|`Cmd`+`Shift`+`R`
自动对齐|`Control`+`Shift`+`J`|`Cmd`+`Shift`+`J`
代码块|`Control`+`Alt`+`C`|`Cmd`+`Alt`+`C`
下标|`Control`+`,`|`Cmd`+`,`
上标|`Control`+`.`|`Cmd`+`.`

### 文本选择
命令|视窗/Linux|苹果系统
---|---|---
全选|`Control`+`A`|`Cmd`+`A`
将所选内容向左扩展一个字符|`Shift`+`←`|`Shift`+`←`
将所选内容向右扩展一个字符|`Shift`+`→`|`Shift`+`→`
将选择扩展到一行|`Shift`+`↑`|`Shift`+`↑`
将选择向下延伸一行|`Shift`+`↓`|`Shift`+`↓`
将选择范围扩展到文档的开头|`Control`+`Shift`+`↑`|`Cmd`+`Shift`+`↑`
将选择范围扩展到文档末尾|`Control`+`Shift`+`↓`|`Cmd`+`Shift`+`↓`

## 覆盖键盘快捷键
键盘快捷键可能是诸如 之类的字符串'`Shift`-`Control`-Enter'。键基于可以出现在 中的字符串event.key，并与 串联起来-。有一个名为keycode.info的小工具，它以交互方式显示event.key。

使用小写字母来表示字母键（如果您希望按住 `Shift` 键，则使用大写字母）。您可以将Space其用作 .

修饰符可以按任何顺序给出。`Shift`、`Alt`、`Control`和`Cmd`被识别。对于通过按住`Shift`创建的字符，`Shift`前缀是隐含的，不应显式添加。

您可以在 Mac 和其他平台上用作Mod简写。`Cmd``Control`

以下是如何覆盖现有扩展的键盘快捷键的示例：

```js
// 1. 导入扩展
import BulletList from '@tiptap/extension-bullet-list'

// 2. 覆盖键盘快捷键
const CustomBulletList = BulletList.extend({
  addKeyboardShortcuts() {
    return {
      // ↓ 新的键盘快捷键
      'Mod-l': () => this.editor.commands.toggleBulletList(),
    }
  },
})

// 3. 将自定义扩展添加到编辑器
new Editor({
  extensions: [
    CustomBulletList(),
    // …
  ],
})
```