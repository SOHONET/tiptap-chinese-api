# 事件

[[toc]]

编辑器会触发一些您可以挂钩的不同事件。让我们先看看所有可用的事件。

## **可用事件列表**

### beforeCreate

在创建视图之前

### create

编辑器准备好了

### update

内容变了

### selectionUpdate

选择已更改

### transaction

编辑器状态已更改

### focus

编辑器获取焦点

### blur

编辑器失去焦点

### destroy

编辑器正在被销毁

## **注册事件监听器**

可以通过三种方式注册事件监听器

### **选项 1：配置**

您可以立即在新的编辑器实例上定义事件侦听器：

```js
const editor = new Editor({
  onBeforeCreate({ editor }) {
    // 在创建视图之前
  },
  onCreate({ editor }) {
    // 编辑器准备好了
  },
  onUpdate({ editor }) {
    // 内容变了
  },
  onSelectionUpdate({ editor }) {
    // 选择已经改变
  },
  onTransaction({ editor, transaction }) {
    // 编辑器状态已更改
  },
  onFocus({ editor, event }) {
    // 编辑获取焦点
  },
  onBlur({ editor, event }) {
    // 编辑失去焦点
  },
  onDestroy() {
    // 编辑器被销毁
  },
})
```

### **选项 2：绑定**

或者您可以在正在运行的编辑器实例上注册您的事件侦听器：

### **绑定事件监听器**

```js
editor.on('beforeCreate', ({ editor }) => {
  // 在创建视图之前
})

editor.on('create', ({ editor }) => {
  // 编辑器准备好了
})

editor.on('update', ({ editor }) => {
  // 内容变了
})

editor.on('selectionUpdate', ({ editor }) => {
  // 选择已经改变
})

editor.on('transaction', ({ editor, transaction }) => {
  // 编辑器状态已更改
})

editor.on('focus', ({ editor, event }) => {
  // 编辑获取焦点
})

editor.on('blur', ({ editor, event }) => {
  // 编辑失去焦点
})

editor.on('destroy', () => {
  // 编辑器被销毁
})
```

### **取消绑定事件监听器**

如果您需要在某个时候解除绑定这些事件侦听器，您应该注册您的事件侦听器`.on()`并在那时解除绑定`.off()`。

```js
const onUpdate = () => {
  // 内容有变化
}

// Bind …
editor.on('update', onUpdate)

// … and unbind.
editor.off('update', onUpdate)
```

### **选项 3：扩展**

也可以将事件侦听器移动到自定义扩展（或节点或标记）。这看起来像这样：

```js
import { Extension } from '@tiptap/core'

const CustomExtension = Extension.create({
  onBeforeCreate({ editor }) {
    // 在创建视图之前
  },
  onCreate({ editor }) {
    // 编辑器准备好了.
  },
  onUpdate({ editor }) {
    // 内容变了.
  },
  onSelectionUpdate({ editor }) {
    // 选择已经改变
  },
  onTransaction({ editor, transaction }) {
    // 编辑器状态已更改.
  },
  onFocus({ editor, event }) {
    // 编辑获取焦点
  },
  onBlur({ editor, event }) {
    // 编辑失去焦点
  },
  onDestroy() {
    // 编辑器被销毁
  },
})
```