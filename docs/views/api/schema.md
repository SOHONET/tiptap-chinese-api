# 模式

[[toc]]

与许多其他编辑器不同，Tiptap 基于定义内容结构的[模式。](https://prosemirror.net/docs/guide/#schema)这使您能够定义文档中可能出现的节点类型、其属性以及它们的嵌套方式。

这个模式*非常*严格。您不能使用任何未在您的架构中定义的 HTML 元素或属性。

让我给你举个例子：如果你将类似的东西粘贴`This is <strong>important</strong>`到 Tiptap 中，但没有任何处理`strong`标签的扩展，你只会看到`This is important`– 没有强标签。

## 模式的样子

当您只使用提供的扩展时，您不必太在意架构。如果您要构建自己的扩展，了解架构的工作原理可能会有所帮助。让我们看一下典型 ProseMirror 编辑器的最简单架构：

```js
// 底层ProseMirror模式
{
  nodes: {
    doc: {
      content: 'block+',
    },
    paragraph: {
      content: 'inline*',
      group: 'block',
      parseDOM: [{ tag: 'p' }],
      toDOM: () => ['p', 0],
    },
    text: {
      group: 'inline',
    },
  },
}
```

我们在这里注册了三个节点。`doc`,`paragraph`和`text`. `doc`是根节点，它允许一个或多个块节点作为子节点 ( `content: 'block+'`)。由于`paragraph`在块节点组 ( `group: 'block'`) 中，我们的文档只能包含段落。我们的段落允许零个或多个内联节点作为子节点 ( `content: 'inline*'`)，因此只能在`text`其中。`parseDOM`定义如何从粘贴的 HTML 中解析节点。`toDOM`定义它将如何在 DOM 中呈现。

在 Tiptap 中，每个节点、标记和扩展名都存在于自己的文件中。这允许我们拆分逻辑。在引擎盖下，整个模式将合并在一起：

```js
// Tiptap模式API
import { Node } from '@tiptap/core'

const Document = Node.create({
  name: 'doc',
  topNode: true,
  content: 'block+',
})

const Paragraph = Node.create({
  name: 'paragraph',
  group: 'block',
  content: 'inline*',
  parseHTML() {
    return [
      { tag: 'p' },
    ]
  },
  renderHTML({ HTMLAttributes }) {
    return ['p', HTMLAttributes, 0]
  },
})

const Text = Node.create({
  name: 'text',
  group: 'inline',
})
```

## Nodes and marks

### 差异

节点就像内容块，例如段落、标题、代码块、块引用等等。

标记可以应用于节点的特定部分。粗体、*斜体*或_~~罢工~~文本。[链接](https://github.com/ueberdosis/tiptap/blob/b0198eb14b98db5ca691bd9bfe698ffaddbc4ded/docs/api/schema.md#)也是标记。

### 节点模式

### Content

content 属性准确定义了节点可以拥有的内容类型。ProseMirror 对此非常严格。这意味着，不符合模式的内容将被丢弃。它需要一个名称或组作为字符串。这里有一些例子：

```js
Node.create({
  // 必须有一个或多个块
  content: 'block+',

  // 必须有零个或多个块
  content: 'block*',

  // 允许所有类型的'inline'内容(文本或硬break)
  content: 'inline*',

  // 只能是'text'
  content: 'text*',

  // 可以有一个或多个段落，或列表(如果使用列表)
  content: '(paragraph|list?)+',

  // 顶部必须有一个标题，下面必须有一个或多个块
  content: 'heading block+'
})
```

### Marks

`marks`您可以使用模式设置定义节点内允许哪些标记。添加一个或多个名称或标记组，允许所有或禁止所有标记，如下所示：

```js
Node.create({
  // 只允许'加粗'标记
  marks: 'bold',

  // 只允许'粗体'和'斜体'标记
  marks: 'bold italic',

  // 允许所有标记
  marks: '_',

  // 禁用所有标记
  marks: '',
})
```

### Group

将此节点添加到一组扩展中，可以在模式的[内容](https://github.com/ueberdosis/tiptap/blob/b0198eb14b98db5ca691bd9bfe698ffaddbc4ded/docs/api/schema.md#content)属性中引用。

```js
Node.create({
  // 添加到block组
  group: 'block',

  // 添加到'inline'组
  group: 'inline',

  // 添加到'block'和'list'组
  group: 'block list',
})
```

### Inline

节点也可以内联呈现。当设置`inline: true`节点与文本一致时。提及就是这种情况。结果更像是一个标记，但具有节点的功能。一个区别是生成的 JSON 文档。一次应用多个标记，内联节点将导致嵌套结构。

```js
Node.create({
  // 例如，呈现与文本一致的节点
  inline: true,
})
```

对于某些您想要标记中不可用的功能的情况，例如节点视图，请尝试内联节点是否可行：

```js
Node.create({
  name: 'customInlineNode',
  group: 'inline',
  inline: true,
  content: 'text*',
})
```

### Atom

带有 的节点`atom: true`不可直接编辑，应将其视为一个单元。它不太可能在编辑器上下文中使用它，但这就是它的样子：

```js
Node.create({
  atom: true,
})
```

一个例子是[Mention](https://github.com/ueberdosis/tiptap/blob/b0198eb14b98db5ca691bd9bfe698ffaddbc4ded/api/nodes/mention)扩展，它在某种程度上看起来像文本，但表现得更像一个单元。因为它没有可编辑的文本内容，所以当你复制这样的节点时它是空的。好消息是，你可以控制它。这是扩展中的示例[Mention](https://github.com/ueberdosis/tiptap/blob/b0198eb14b98db5ca691bd9bfe698ffaddbc4ded/api/nodes/mention)：

```js
// 用于将原子节点转换为纯文本
renderText({ node }) {
  return `@${node.attrs.id}`
},
```

### Selectable

除了已经可见的文本选择之外，还有一个不可见的节点选择。如果你想让你的节点可选，你可以这样配置：

```js
Node.create({
  selectable: true,
})
```

### Draggable

使用此设置可以将所有节点配置为可拖动（默认情况下它们不是）：

```js
Node.create({
  draggable: true,
})
```

### Code

用户期望代码的行为非常不同。对于包含代码的所有类型的节点，您可以设置`code: true`为考虑到这一点。

```js
Node.create({
  code: true,
})
```

### Whitespace

控制解析此 a 节点中空白的方式。

```js
Node.create({
  whitespace: 'pre',
})
```

### Defining

默认情况下，当节点的全部内容被替换时（例如，粘贴新内容时），节点会被删除。如果应该为此类替换操作保留一个节点，请将它们配置为`defining`.

通常，这适用于[Blockquote](https://github.com/ueberdosis/tiptap/blob/b0198eb14b98db5ca691bd9bfe698ffaddbc4ded/api/nodes/blockquote)、[CodeBlock](https://github.com/ueberdosis/tiptap/blob/b0198eb14b98db5ca691bd9bfe698ffaddbc4ded/api/nodes/code-block)、[Heading](https://github.com/ueberdosis/tiptap/blob/b0198eb14b98db5ca691bd9bfe698ffaddbc4ded/api/nodes/heading)和[ListItem](https://github.com/ueberdosis/tiptap/blob/b0198eb14b98db5ca691bd9bfe698ffaddbc4ded/api/nodes/list-item)。

```js
Node.create({
  defining: true,
})
```

### Isolating

对于应该将光标围起来以进行退格等常规编辑操作的节点，例如 TableCell，设置`isolating: true`.

```js
Node.create({
  isolating: true,
})
```

### Allow gap cursor

该[Gapcursor](https://github.com/ueberdosis/tiptap/blob/b0198eb14b98db5ca691bd9bfe698ffaddbc4ded/api/extensions/gapcursor)扩展注册了一个新的模式属性来控制是否允许在该节点的任何地方使用间隙游标。

```js
Node.create({
  allowGapCursor: false,
})
```

### Table roles

该[Table](https://github.com/ueberdosis/tiptap/blob/b0198eb14b98db5ca691bd9bfe698ffaddbc4ded/api/nodes/table)扩展注册了一个新的模式属性来配置节点具有的角色。允许的值为`table`、`row`、`cell`和`header_cell`。

```js
Node.create({
  tableRole: 'cell',
})
```

### The mark schema

### Inclusive

如果您不希望标记在光标结束时处于活动状态，请将 inclusive 设置为`false`。例如，这就是它为[Link](https://github.com/ueberdosis/tiptap/blob/b0198eb14b98db5ca691bd9bfe698ffaddbc4ded/api/marks/link)标记配置的方式：

```js
Mark.create({
  inclusive: false,
})
```

### Excludes

默认情况下，可以同时应用所有节点。使用 excludes 属性，您可以定义哪些标记不能与标记共存。例如，内联代码标记排除任何其他标记（粗体、斜体和所有其他标记）。

```js
Mark.create({
  // 不能与加粗标记共存
  excludes: 'bold'
  // 排除任何其他标记
  excludes: '_',
})
```

### Exitable

默认情况下，标记会“捕获”光标，这意味着光标无法离开标记，除非将光标从左向右移动到没有标记的文本中。如果设置为 true，当标记位于节点末尾时，标记将退出。例如使用代码标记这很方便。

```js
Mark.create({
  // 使此标记可退出-默认值为false
  exitable: true,
})
```

### Group

将此标记添加到一组扩展中，可以在模式的内容属性中引用。

```js
Mark.create({
  // 将此标记添加到'basic'组
  group: 'basic',
  // 将此标记添加到'basic'组和'foobar'组
  group: 'basic foobar',
})
```

### Code

用户期望代码的行为非常不同。对于各种包含代码的标记，您可以设置`code: true`为考虑到这一点。

```js
Mark.create({
  code: true,
})
```

### Spanning

默认情况下，标记在呈现为 HTML 时可以跨越多个节点。设置`spanning: false`以指示标记不得跨越多个节点。

```js
Mark.create({
  spanning: false,
})
```

## 获取底层 ProseMirror 架构

在一些用例中，您需要使用底层架构。如果您正在使用 Tiptap 协作文本编辑功能，或者如果您想要手动将您的内容呈现为 HTML，您将需要它。****

### 选项 1：与编辑器

如果你在客户端需要这个并且无论如何都需要一个编辑器实例，它可以通过编辑器获得：

```js
import { Editor } from '@tiptap/core'
import Document from '@tiptap/extension-document'
import Paragraph from '@tiptap/extension-paragraph'
import Text from '@tiptap/extension-text'

const editor = new Editor({
  extensions: [
    Document,
    Paragraph,
    Text,
    // 在这里添加更多扩展
  ])
})

const schema = editor.schema
```

### 选项 2：没有编辑器

如果您只想拥有架构*而不*初始化实际的编辑器，则可以使用`getSchema`辅助函数。它需要一系列可用的扩展，并为您方便地生成一个 ProseMirror 模式：