# 访问 ProseMirror 内部结构

[[toc]]

Tiptap 构建在 ProseMirror 之上，后者拥有非常强大的 API。为了访问它，我们提供了包@tiptap/pm。该软件包提供了所有重要的 ProseMirror 软件包，例如prosemirror-state、prosemirror-view或prosemirror-model。使用该包进行自定义开发可确保您始终拥有 Tiptap 所使用的相同版本的 ProseMirror。这样，我们可以确保 Tiptap 和所有扩展相互兼容并防止版本冲突。另一个优点是您不需要手动安装所有 ProseMirror 软件包，特别是如果您不使用 npm 或任何其他支持自动对等依赖关系解析的软件包管理器。

**安装:**

```shell
npm i @tiptap/pm
```
之后，您可以像这样访问所有内部 ProseMirror 包：

```js
// 这个例子从ProseMirror状态包中加载EditorState类
import { EditorState } from '@tiptap/pm/state'
```
可以使用以下软件包：

## `@tiptap/pm/changeset`

这是一个帮助模块，可以将一系列文档更改转换为一组插入和删除，例如将它们显示在更改跟踪界面中。这样的集合可以增量地建立，以便在实时编辑期间以半性能的方式进行这样的更改跟踪。

**编程接口**

插入和删除表示为“跨度”——文档中的范围。删除的跨度引用原始文档，而插入的跨度指向当前文档。

可以将任意数据值与此类跨度相关联，例如跟踪进行更改的用户、进行更改的时间戳或再次反转所需的步骤数据。

#### `class Change<Data = any>`

具有与其关联的元数据的替换范围。

- `fromA: number`

    旧文档中已删除/替换的范围的开头。

- `toA: number`

    旧文档中范围的末尾。

- `fromB: number`

    新文档中插入的范围的开始位置。

- `toB: number`

    新文档中范围的末尾。

- `deleted: readonly Span[]`

    与已删除内容相关的数据。这些跨度的长度加起来为`this.toA - this.fromA`。

- `inserted: readonly Span[]`

    与插入内容相关的数据。长度总计为 `this.toB - this.toA`.

- `static merge<Data>(x: readonly Change[], y: readonly Change[], combine: fn(dataA: Data, dataB: Data) → Data) → readonly Change[]`

    这会将两个变更集（x 的结束文档应该是 y 的开始文档）合并为一个跨越 x 的开头到 y 的结尾的变更集。


#### `class Span<Data = any>`

存储部分更改的元数据。

- `length: number`

    这个跨度的长度。

- `data: Data`

    与此跨度关联的数据。


#### `class ChangeSet<Data = any>`

更改集跟踪从过去的给定点开始对文档的更改。它将许多步骤图压缩为平坦的替换序列，并简化了通过比较内容而部分撤销的替换。

- `changes: readonly Change[]`

    替换的区域。

- `addSteps(newDoc: Node, maps: readonly StepMap[], data: Data | readonly Data[]) → ChangeSet`

    通过将给定的步骤映射和元数据（作为数组、每个映射或作为与所有映射关联的单个值）添加到当前集合来计算新的变更集。不会改变旧的集合。

    请注意，由于每次添加后都会进行简化，因此增量添加步骤可能会创建与一次添加所有这些更改不同的最终集，因为在简化过程中可能会根据当前更改范围的边界来匹配不同的文档标记。

- `startDoc: Node`

    变更集的起始文档。

- `map(f: fn(range: Span) → Data) → ChangeSet`

    通过函数映射给定集合中跨度的数据值，并使用结果数据构造一个新集合。

- `changedRange(b: ChangeSet, maps?: readonly StepMap[]) → {from: number, to: number}`

    比较两个变更集并返回它们更改的范围（如果有）。如果文档在映射之间发生更改，请将更改它的步骤的映射作为第二个参数传递，并确保在旧集上调用该方法并传递新集。返回的位置将位于新文档坐标中。

- `static create<Data = any>(doc: Node, combine?: fn(dataA: Data, dataB: Data) → Data = (a, b) => a === b ? a : null as any) → ChangeSet`

    使用给定的基础对象和配置创建变更集。该`combine`函数用于比较和组合元数据 - 当元数据不兼容时，它应该返回 null；当元数据不兼容时，它应该返回合并范围的组合版本。

- `simplifyChanges(changes: readonly Change[], doc: Node) → Change[]`

    简化了一组演示更改。这使得假设在一个单词中同时存在插入和删除是令人困惑的，并且当发生此类更改而它们之间没有单词边界时，它们应该扩展以覆盖它们接触的整个单词集（在新文档中）。单字符替换除外。

## `@tiptap/pm/collab`

这是ProseMirror的核心模块。ProseMirror 是一个基于 contentEditable 的功能良好的富语义内容编辑器，支持协作编辑和自定义文档模式。

该模块实现了一个插件，可以帮助跟踪和合并更改以进行 协作编辑。

该模块实现了一个 API，可以将协作编辑的通信通道连接到该 API。有关更多详细信息和示例，请参阅 指南。

- `collab(config⁠?: Object = {}) → Plugin`

  创建一个插件，为编辑器启用协作编辑框架。

  **config**
    - version⁠?: number  协同编辑的起始版本号。默认为 0。
    - clientID⁠?: number | string  该客户端的ID，用于区分该客户端的变化与其他客户端的变化。默认为随机 32 位数字。

- `getVersion(state: EditorState) → number`

  获取协作插件已与中央机构同步的最新版本。
-
  `
  receiveTransaction(
    state: EditorState,
    steps: readonly Step[],
    clientIDs: readonly (string | number)[],
    options⁠?: Object = {}
  ) → Transaction
  `

  创建一个交易，代表从权威机构收到的一组新步骤。应用此交易将推动国家向前调整以适应当局对该文件的看法。

  **options**
    - mapSelectionBackward⁠?: boolean  启用后（默认为false），如果当前选择是文本选择，则其两侧将针对此事务以负偏差进行映射，以便在光标处插入的内容在光标之后结束。用户通常更喜欢这样做，但出于向后兼容性的原因，默认情况下不会这样做。

-
  `
  sendableSteps(state: EditorState) → {
    version: number,
    steps: readonly Step[],
    clientID: number | string,
    origins: readonly Transaction[]
  }
  `

  提供描述编辑者未经确认的步骤的数据，需要将其发送给中央机构。当没有什么可发送时返回 null。

  **origins**

  保存产生每个步骤的原始交易。这对于查找步骤的时间戳和其他元数据非常有用，但请注意，这些步骤可能已重新设置基础，而原始事务仍然是旧的、未更改的对象。

## `@tiptap/pm/commands`

该模块导出许多命令，这些命令是封装编辑操作的构建块函数。命令函数采用编辑器状态、可选的dispatch可用于分派事务的函数和可选的实例EditorView。它应该返回一个布尔值，指示它是否可以执行任何操作。当没有dispatch传递回调时，该命令应该执行“空运行”，确定它是否适用，但实际上不执行任何操作。

它们主要用于绑定按键和定义菜单项。

-
  `
  type Command = fn(
    state: EditorState,
    dispatch⁠?: fn(tr: Transaction),
    view⁠?: EditorView
  ) → boolean
  `

  命令是接受状态和可选事务调度函数的函数，并且......
    - 确定它们是否适用于该州
    - 如果不是，则返回 false
    - 如果dispatch已通过，则执行其效果，可能通过将交易传递给dispatch
    - 返回true

  在某些情况下，编辑器视图作为第三个参数传递。

-
  `chainCommands(...commands: readonly Command[]) → Command`

  将多个命令函数组合成一个函数（逐一调用它们，直到有一个函数返回 true）。

-
  `deleteSelection: Command`

  删除选择（如果有）。

-
  `joinBackward: Command`

  如果所选内容为空并且位于文本块的开头，请尝试缩短该块与其前面的块之间的距离 - 如果直接在其之前有一个可以连接的块，请将它们连接起来。如果不是，请尝试将所选块移至文档结构中的下一个块，方法是将其从其父块中移出或将其移动到前一个块的父块中。如果给定，将使用该视图进行准确的（双向感知的）文本块开始检测。

-
  `selectNodeBackward: Command`

  当选择为空且位于文本块的开头时，如果可能，请选择该文本块之前的节点。这是为了绑定到诸如退格键、后键 joinBackward或其他删除命令之类的键，作为架构不允许在选定点进行删除时的后备行为。

-
  `joinTextblockBackward: Command`

  如果光标位于文本块的开头，则一种更有限的形式joinBackward 仅尝试将当前文本块连接到其之前的文本块。

-
  `joinForward: Command`

  如果选择为空并且光标位于文本块的末尾，请尝试减少或删除该块与其后一个块之间的边界，方法是连接它们或在树结构中将另一个块移近该块。如果给定，将使用该视图进行准确的文本块开始检测。

-
  `selectNodeForward: Command`

  当选择为空且位于文本块末尾时，如果可能，请选择该文本块之后的节点。其目的是绑定到诸如删除、之后 joinForward和类似删除命令之类的键，以便在架构不允许在选定点进行删除时提供后备行为。

-
  `joinTextblockForward: Command`

  joinForward 如果光标位于文本块的末尾，则一种更有限的形式仅尝试将当前文本块连接到其后的文本块。

-
  `joinUp: Command`

  连接选定的块，或者，如果存在文本选择，则连接可以连接的选择的最近的祖先块，其同级块位于其上方。

-
  `joinDown: Command`

  连接选定的块或可以连接的选择的最近祖先，其后是同级块。

-
  `lift: Command`

  将选定的块或可以提升的选择的最接近的祖先块从其父节点中提升出来。

-
  `newlineInCode: Command`

  如果所选内容所在的节点的类型 code在其规范中具有 true 属性，则将所选内容替换为换行符。

-
  `exitCode: Command`

  当选择位于 code其规范中具有 true 属性的节点中时，在代码块之后创建一个默认块，并将光标移动到那里。

-
  `createParagraphNear: Command`

  如果选择了块节点，请在其之前（如果它是其父级的第一个子级）或之后创建一个空段落。

-
  `liftEmptyBlock: Command`

  如果光标位于可以抬起的空文本块中，请抬起该块。

-
  `splitBlock: Command`

  分割所选内容的父块。如果选择是文本选择，也删除其内容。

-
  `
  splitBlockAs(
    splitNode⁠?: fn(node: Node, atEnd: boolean) → {type: NodeType, attrs⁠?: Attrs}
  ) → Command
  `

  创建一个变体splitBlock，使用自定义函数来确定新拆分块的类型。

-
  `splitBlockKeepMarks: Command`

  作用类似于splitBlock，但不重置光标处的活动标记集。

-
  `selectParentNode: Command`

  将选择移动到包含当前选择的节点（如果有）。（不会选择文档节点。）

-
  `selectAll: Command`

  选择整个文档。

-
  `selectTextblockStart: Command`

  将光标移动到当前文本块的开头。

-
  `selectTextblockEnd: Command`

  将光标移动到当前文本块的末尾。

-
  `wrapIn(nodeType: NodeType, attrs⁠?: Attrs = null) → Command`

  将选择内容包装在具有给定属性的给定类型的节点中。

-
  `setBlockType(nodeType: NodeType, attrs⁠?: Attrs = null) → Command`

  返回一个命令，尝试将选定的文本块设置为具有给定属性的给定节点类型。

-
  `toggleMark(markType: MarkType, attrs⁠?: Attrs = null) → Command`

  创建一个命令函数，用给定的属性切换给定的标记。false当当前选择不支持该标记时将返回。如果选择中存在该类型的任何标记，这将删除该标记，否则添加它。如果选择为空，则这适用于存储的标记而不是文档的范围。

-
  `
  autoJoin(
    command: Command,
    isJoinable: fn(before: Node, after: Node) → boolean |
    readonly string[]
  ) → Command
  `

  包装命令，以便当它产生导致两个可连接节点最终彼此相邻的转换时，这些节点将被连接。当节点具有相同类型并且isJoinable谓词为它们返回 true 时，或者如果传递了字符串数组且它们的节点类型名称位于该数组中，则节点被视为可连接。

-
  `baseKeymap: Object<Command>`

  根据检测到的平台，这将保持 pcBasekeymap或 macBaseKeymap。

-
  `pcBaseKeymap: Object<Command>`

  包含不特定于任何架构的绑定的基本键盘映射。绑定以下键（当列出多个命令时，它们用 链接chainCommands）：

    - Enter to newlineInCode, createParagraphNear, liftEmptyBlock, splitBlock
    - Mod-Enter to exitCode
    - Backspace and Mod-Backspace to deleteSelection, joinBackward, selectNodeBackward
    - Delete and Mod-Delete to deleteSelection, joinForward, selectNodeForward
    - Mod-Delete to deleteSelection, joinForward, selectNodeForward
    - Mod-a to selectAll

-
  `macBaseKeymap: Object<Command>`

  该副本pcBaseKeymap还绑定Ctrl-h（如 Backspace）、 Ctrl-d（如删除）、Alt-Backspace（如 Ctrl-Backspace）以及 Ctrl-Alt-Backspace、Alt-Delete和Alt-d（如 Ctrl-Delete）。

## `@tiptap/pm/dropcursor`

该模块实现了一个插件，可显示 ProseMirror 的下拉光标。

-
  `dropCursor(options?: interface = {}) → Plugin`

  创建一个插件，当添加到 ProseMirror 实例时，当在编辑器上拖动某些内容时，该插件会导致装饰显示在放置位置。

  节点可以在其规范中添加一个disableDropCursor属性来控制其中放置光标的显示。这可能是一个布尔值或一个函数，将使用视图、位置和 DragEvent 来调用，并且应该返回一个布尔值。

  **options**

    - color?: string
      光标的颜色。默认为black.
    - width?: number
      光标的精确宽度（以像素为单位）。默认为 1。
    - class?: string
      要添加到光标元素的 CSS 类名称。

## `@tiptap/pm/gapcursor`

它添加了一种选择类型，用于聚焦不允许常规选择的位置（例如在其前后都有叶块节点、表格或文档末尾的位置）。

您可能需要加载style/gapcursor.css，其中包含模拟光标的基本样式（作为短的、闪烁的水平条纹）。

默认情况下，仅允许在默认内容节点（在模式内容约束中）是文本块节点的位置使用间隙游标。allowGapCursor您可以通过向节点规范添加属性来自定义此设置- 如果为真，则该节点中的任何位置都允许间隙光标，如果是false则不允许。

-
  `gapCursor() → Plugin`

  创建一个间隙光标插件。启用后，这将捕获附近没有通常可选择位置的位置附近的点击和箭头键移动过去的位置，并为它们创建间隙光标选择。光标被绘制为具有 class 的元素 ProseMirror-gapcursor。您可以从包的目录中包含 style/gapcursor.css或添加您自己的样式以使其可见。

-
  `class GapCursor extends Selection`

  间隙光标选择使用此类表示。它 $anchor和$head属性都指向光标位置。

  `new GapCursor($pos: ResolvedPos)`
    创建间隙光标。

## `@tiptap/pm/history`

ProseMirror 的撤消/重做历史记录的实现。此历史记录是选择性的，这意味着它不仅可以回滚到之前的状态，还可以撤消某些更改，同时保持其他后续更改不变。（这对于协作编辑是必要的，并且在其他情况下也会出现。）

-
  `history(config⁠?: Object = {}) → Plugin`

  返回一个启用编辑器撤消历史记录的插件。该插件将跟踪撤消和重做堆栈，可以与 undo和redo命令一起使用。

  您可以在事务上设置"addToHistory" 元数据属性，false以防止它被撤消回滚。

  **config**

    - `depth⁠?: number`

      在丢弃最旧的事件之前收集的历史事件的数量。默认为 100。

    - `newGroupDelay⁠?: number`

      更改之间的延迟，之后应开始新组。默认为 500（毫秒）。请注意，当更改不相邻时，始终会启动一个新组。

-
  `undo: Command`

  撤消最后更改（如果有）的命令函数。

-
  `redo: Command`

  重做上次撤消的更改（如果有）的命令函数。

-
  `undoDepth(state: EditorState) → any`

  给定状态下可用的可撤消事件的数量。

-
  `redoDepth(state: EditorState) → any`

  给定编辑器状态下可用的可重做事件的数量。

-
  `closeHistory(tr: Transaction) → Transaction`

  在给定事务上设置一个标志，以防止将进一步的步骤附加到现有历史事件中（以便它们需要单独的撤消命令来撤消）。

## `@tiptap/pm/inputrules`

用于将输入规则附加到编辑器，该编辑器可以对用户键入的文本做出反应或转换。它还附带了一系列可以在此插件中启用的默认规则。

-
  `class InputRule`

  输入规则是描述一段文本的正则表达式，当键入该文本时，会导致某些事情发生。这可能会将两个破折号更改为一个破折号，将一个以 开头的段落包装成"> "一个块引用，或者完全不同的东西。

  ```
  new InputRule(
    match: RegExp,
    handler: string |
      fn(
        state: EditorState,
        match: RegExpMatchArray,
        start: number,
        end: number
      ) → Transaction
  )
  ```

  创建输入规则。当用户键入内容并且光标前面的文本匹配时 match，该规则适用，该文本应以 结尾$。

  可以handler是字符串，在这种情况下，匹配的文本或正则表达式中的第一个匹配的组将被该字符串替换。

  或者它可以是一个函数，它将使用 生成的匹配数组 RegExp.exec以及匹配范围的开始和结束来调用，并且可以返回描述规则效果的事务，或者返回 null 以指示输入是没有处理。

-
  `
  inputRules({rules: readonly InputRule[]}) → Plugin<{transform: Transaction, from: number, to: number, text: string}>
  `

  创建输入规则插件。启用后，它将导致与任何给定规则匹配的文本输入触发规则的操作。

-
  `undoInputRule: Command`

  如果应用这样的规则是用户所做的最后一件事，则该命令将撤消输入规则。

  该模块附带了许多预定义的规则：

-
  `emDash: InputRule`

  将双破折号转换为长破折号。

-
  `ellipsis: InputRule`

  将三个点转换为省略号字符。

-
  `openDoubleQuote: InputRule`

  “智能”打开双引号。

-
  `closeDoubleQuote: InputRule`

  “智能”关闭双引号。

-
  `openSingleQuote: InputRule`

  “智能”打开单引号。

-
  `closeSingleQuote: InputRule`

  “智能”关闭单引号。

-
  `smartQuotes: readonly InputRule[]`

  智能报价相关的输入规则。

  这些实用程序函数采用特定于模式的参数并创建特定于该模式的输入规则。

-
  `
  wrappingInputRule(
    regexp: RegExp,
    nodeType: NodeType,
    getAttrs⁠?: Attrs |
    fn(matches: RegExpMatchArray) → Attrs |
      null
    = null,
    joinPredicate⁠?: fn(match: RegExpMatchArray, node: Node) → boolean
  ) → InputRule
  `

  构建输入规则，以便在键入给定字符串时自动换行文本块。参数regexp直接传递给InputRule构造函数。您可能希望正则表达式以 开头^，以便该模式只能出现在文本块的开头。

  nodeType是要包装的节点的类型。如果需要属性，您可以直接传递它们，也可以传递一个函数，该函数将从正则表达式匹配中计算它们。

  默认情况下，如果新包装的节点上方有一个类型相同的节点，则规则将尝试连接这两个节点。您可以传递一个连接谓词，该谓词采用正则表达式匹配和包装节点之前的节点，并且可以返回一个布尔值来指示是否应该发生连接。

-
  `
  textblockTypeInputRule(
    regexp: RegExp,
    nodeType: NodeType,
    getAttrs⁠?: Attrs |
    fn(match: RegExpMatchArray) → Attrs |
      null
    = null
  ) → InputRule
  `

  构建一个输入规则，当匹配的文本输入到文本块中时，该规则会更改文本块的类型。您通常希望^以它仅在文本块的开头匹配的方式来启动正则表达式。可选getAttrs参数可用于计算新节点的属性，其工作方式与函数中相同 wrappingInputRule。

## `@tiptap/pm/keymap`

用于方便定义键绑定的插件。

-
  `keymap(bindings: Object<Command>) → Plugin`

  为给定的一组绑定创建一个键盘映射插件。

  绑定应将键名称映射到命令样式的函数，该函数将使用(EditorState, dispatch, EditorView)参数进行调用，并且在处理该键时应返回 true。请注意，视图参数不是命令协议的一部分，但如果绑定需要直接与 UI 交互，则可以将其用作逃生口。

  键名称可以是字符串，例如"Shift-Ctrl-Enter"以零个或多个修饰符为前缀的键标识符。键标识符基于可以出现在 中的字符串 KeyEvent.key。使用小写字母来表示字母键（如果您希望按住 Shift 键，则使用大写字母）。您可以用作"Space"该" "名称的别名。

  修饰符可以按任何顺序给出。Shift-(或s-)、Alt-(或 a-)、Ctrl-(或c-或Control-) 和Cmd-(或m-或 Meta-) 被识别。对于通过按住shift创建的字符，Shift-前缀是隐含的，不应显式添加。

  您可以在 Mac 和其他平台上用作Mod-简写。Cmd-Ctrl-

  您可以向编辑器添加多个键盘映射插件。它们出现的顺序决定了它们的优先级（数组中靠前的那些首先被调度）。

-
  `keydownHandler(bindings: Object<Command>) → fn(view: EditorView, event: KeyboardEvent) → boolean`

  给定一组绑定（使用与 相同的格式 keymap），返回处理它们的keydown 处理程序。

## `@tiptap/pm/markdown`

该模块实现了与CommonMark使用的文档架构相对应的ProseMirror架构，以及用于在该架构中的 ProseMirror 文档与 CommonMark/Markdown 文本之间进行转换的解析器和序列化器。

schema: `Schema<"doc" | "paragraph" | "blockquote" | "horizontal_rule" | "heading" | "code_block" | "ordered_list" | "bullet_list" | "list_item" | "text" | "image" | "hard_break", "em" | "strong" | "link" | "code">`

CommonMark 使用的数据模型的文档架构。

#### class MarkdownParser

Markdown 解析器的配置。这样的解析器使用 markdown-it来标记文件，然后运行针对标记给出的自定义规则来创建 ProseMirror 文档树。

-
  `new MarkdownParser(schema: Schema, tokenizer: any, tokens: Object<ParseSpec>)`

  使用给定的配置创建一个解析器。您可以配置 markdown-it 解析器来解析所需的方言，并提供这些标记在对象中映射到的 ProseMirror 实体的描述，该描述将tokens标记名称映射到如何处理它们的描述。这样的描述是一个对象，并且可能具有以下属性：

  - `schema: Schema`

    解析器的文档架构。

  - `tokenizer: any`

    该解析器的 markdown-it 分词器。

  - `tokens: Object<ParseSpec>`

    tokens用于构造此解析器的对象的值。复制和修改以基于其他解析器可能很有用。

  - `parse(text: string) → any`

    将字符串解析为CommonMark标记，并按照此解析器的规则创建 ProseMirror 文档。

#### interface ParseSpec

  用于指定如何解析 Markdown 标记的对象类型。

-
  `node⁠?: string`

  该标记映射到单个节点，可以在给定名称下的模式中查找其类型。必须设置node、 block、 或之一。mark

-
  `block⁠?: string`

  该令牌（除非noCloseToken为真）出现_open 并具有_close变体（附加到基本令牌名称提供对象属性），并包装内容块。该块应包装在由属性值命名的类型的节点中。如果令牌没有 _open或_close，请使用该noCloseToken选项。

-
  `mark⁠?: string`

  该标记（再次强调，除非noCloseToken为真）也有_open变_close体，但应向其内容添加标记（由值命名），而不是将其包装在节点中。

-
  `attrs⁠?: Attrs`

  节点或标记的属性。当getAttrs提供时，它优先。

-
  `getAttrs⁠?: fn(token: any, tokenStream: any[], index: number) → Attrs`

  用于计算节点或标记的属性的函数，它采用markdown-it 标记并返回属性对象。

-
  `noCloseToken⁠?: boolean`

  指示markdown-it 令牌没有_open或_close为节点。这默认为true for code_inline,code_block和fence。

-
  `ignore⁠?: boolean`

  如果为 true，则忽略匹配令牌的内容。

-
  `defaultMarkdownParser: MarkdownParser`

  解析器解析未扩展的CommonMark，没有内联 HTML，并在基本模式中生成文档。

#### class MarkdownSerializer

将 ProseMirror 文档序列化为 Markdown/CommonMark 文本的规范。

-
  `new MarkdownSerializer(nodes: Object<fn(state: MarkdownSerializerState, node: Node, parent: Node, index: number)>, marks: Object<Object>, options⁠?: Object = {})`

  使用给定的配置构造一个序列化器。该nodes 对象应将给定模式中的节点名称映射到采用序列化器状态和此类节点的函数，并序列化该节点。

  **options**

    `escapeExtraCharacters⁠?: RegExp`

    可以添加额外的字符进行转义。它被直接传递给 String.replace()，并且匹配的字符前面有一个反斜杠。

-
  `nodes: Object<fn(state: MarkdownSerializerState, node: Node, parent: Node, index: number)>`

  节点串行器为此串行器起作用。

-
  `marks: Object<Object>`

  标记序列化器信息。

  options: Object

    `escapeExtraCharacters⁠?: RegExp`

    可以添加额外的字符进行转义。它被直接传递给 String.replace()，并且匹配的字符前面有一个反斜杠。

-
  `serialize(content: Node, options⁠?: Object = {}) → string`

  将给定节点的内容序列化为 CommonMark。

  options

    `tightLists⁠?: boolean`

    是否以紧凑的风格渲染列表。通过在节点上指定紧密属性，可以在节点级别上覆盖这一点。默认为 false。

#### class MarkdownSerializerState

这是一个用于跟踪状态并公开与 Markdown 序列化相关的方法的对象。实例被传递到节点和标记序列化方法（请参阅 参考资料toMarkdown）。

-
  `options: {tightLists⁠?: boolean, escapeExtraCharacters⁠?: RegExp}`

  传递给序列化器的选项。

-
  `wrapBlock(delim: string, firstDelim: string, node: Node, f: fn())`

  渲染一个块，为每行添加前缀delim，第一行添加为firstDelim。node应该是块末尾关闭的节点，f是渲染块内容的函数。

-
  `ensureNewLine()`

  确保当前内容以换行符结尾。

-
  `write(content⁠?: string)`

  准备写入输出的状态（关闭闭合段落、添加分隔符等），然后可以选择将内容（未转义）添加到输出。

-
  `closeBlock(node: Node)`

  关闭给定节点的块。

-
  `text(text: string, escape⁠?: boolean = true)`

  将给定文本添加到文档中。当 escape 不存在时false，就会进行 escape。

-
  `render(node: Node, parent: Node, index: number)`

  将给定节点渲染为块。

-
  `renderContent(parent: Node)`

  将 的内容渲染parent为块节点。

-
  `renderInline(parent: Node)`

  将 的内容渲染parent为内联内容。

-
  `renderList(node: Node, delim: string, firstDelim: fn(index: number) → string)`

  将节点的内容呈现为列表。delim应该是添加到项目中除第一行之外的所有行的额外缩进， firstDelim是从项目索引到项目第一行的分隔符的函数。

-
  `esc(str: string, startOfLine⁠?: boolean = false) → string`

  转义给定的字符串，以便它可以安全地出现在 Markdown 内容中。如果startOfLine为 true，则还转义仅在行开头具有特殊含义的字符。

-
  `repeat(str: string, n: number) → string`

  重复给定的字符串n次数。

-
  `markString(mark: Mark, open: boolean, parent: Node, index: number) → string`

  获取给定开始或结束标记的降价字符串。

-
  `getEnclosingWhitespace(text: string) → {leading⁠?: string, trailing⁠?: string}`

  从字符串中获取前导和尾随空格。如果不匹配，则返回对象的前导或尾随属性的值将是未定义的。

-
  `defaultMarkdownSerializer: MarkdownSerializer`

  基本模式的序列化器。

## `@tiptap/pm/menu`

该模块定义了为 ProseMirror 编辑器构建菜单的抽象，以及菜单栏的实现。

该模块定义了 ProseMirror 菜单的许多构建块以及菜单栏实现。

使用此模块时，您应该确保其 style/menu.css 文件已加载到您的页面中。

### 界面菜单元素

此模块中定义的类型并不是您可以在菜单中显示的唯一内容。任何符合此接口的内容都可以放入菜单结构中。

-
  `render(pm: EditorView) → {dom: HTMLElement, update: fn(state: EditorState) → boolean}`

  渲染元素以在菜单中显示。必须返回一个 DOM 元素和一个可用于将该元素更新到新状态的函数。update如果更新隐藏了整个元素，该函数必须返回 false。

### 菜单项类

实现MenuElement单击时执行命令的图标或标签。

  -
    `new MenuItem(spec: MenuItemSpec)`

    创建一个菜单项。

  -
    `spec: MenuItemSpec`

    用于创建此项目的规格。

  -
    `render(view: EditorView) → {dom: HTMLElement, update: fn(state: EditorState) → boolean}`

    根据其显示规范渲染图标，并添加一个事件处理程序，该事件处理程序在单击表示时执行命令。

### 接口菜单项规范

传递给构造函数的配置对象MenuItem。

-
  `run(state: EditorState, dispatch: fn(tr: Transaction), view: EditorView, event: Event)`

  激活菜单项时执行的函数。

-
  `select: ?fn(state: EditorState) → boolean`

  可选函数，用于确定该项目当前是否合适。取消选择的项目将被隐藏。

-
  `enable: ?fn(state: EditorState) → boolean`

  用于确定该项目是否启用的函数。如果给出并返回 false，该项目将被赋予禁用样式。

-
  `active: ?fn(state: EditorState) → boolean`

  用于确定项目是否“活动”的谓词函数（例如，用于切换强标记的项目可能处于活动状态，然后光标位于强文本中）。

-
  `render: ?fn(view: EditorView) → HTMLElement`

  渲染项目的函数。您必须提供此、 icon或label。

-
  `icon: ?IconSpec`

  描述该项目显示的图标。

-
  `label: ?string`

  使项目显示为文本标签。对于包含在下拉菜单或类似菜单中的项目最有用。该对象应该有一个label提供要显示的文本的属性。

-
  `title: ?string | fn(state: EditorState) → string`

  定义项目的 DOM 标题（鼠标悬停）文本。

-
  `class: ?string`

  可以选择将 CSS 类添加到项目的 DOM 表示中。

-
  `css: ?string`

  可以选择将内联 CSS 字符串添加到项目的 DOM 表示中。

-
  `type`
  指定图标。可以是 SVG 图标，在这种情况下，其 属性应该是SVG 路径规范，并且应该提供该路径所在的视图框。或者，它可能有一个指定组成图标的文本字符串的属性，以及一个为文本提供附加 CSS 样式的可选属性。或者它可能包含包含 DOM 节点的属性。`IconSpec  = {path: string, width: number, height: number} | {text: string, css?: ?string} | {dom: Node}` `path` `width` `height` `text` `css` `dom`

### 类下拉菜单
实现MenuElement一个下拉菜单，显示为标签，其右侧有一个向下的三角形。

-
  `new Dropdown(content: readonly MenuElement[] | MenuElement, options: ?Object = {})`

  创建一个包含元素的下拉列表。

-
  `render(view: EditorView) → {dom: HTMLElement, update: fn(state: EditorState) → boolean}`

  渲染下拉菜单和子项目。

### 类下拉子菜单
实现MenuElement表示一个包含一组元素的子菜单，这些元素在悬停或点击时开始隐藏并向右展开。

-
  `new DropdownSubmenu(content: readonly MenuElement[] | MenuElement, options: ?Object = {})`

  为给定的菜单元素组创建子菜单。可以识别以下选项：

-
  `render(view: EditorView) → {dom: HTMLElement, update: fn(state: EditorState) → boolean}`

  呈现子菜单。

-
  `menuBar(options: Object) → Plugin`

  一个将菜单栏放置在编辑器上方的插件。请注意，这涉及将编辑器包装在附加的`<div>`.

  该模块导出以下预构建项或项构造函数：

-
  `joinUpItem: MenuItem`

  命令的菜单项joinUp。

-
  `liftItem: MenuItem`

  命令的菜单项lift。

-
  `selectParentNodeItem: MenuItem`

  命令的菜单项selectParentNode。

-
  `undoItem: MenuItem`

  命令的菜单项undo。

-
  `redoItem: MenuItem`

  命令的菜单项redo。

-
  `wrapItem(nodeType: NodeType, options: Partial & {attrs?: ?Attrs}) → MenuItem`

  构建一个菜单项，用于将选择内容包装在给定的节点类型中。向 中存在的内容 添加run和属性。可以是为包装节点提供属性的对象。`select` `options` `options.attrs`

-
  `blockTypeItem(nodeType: NodeType, options: Partial & {attrs?: ?Attrs}) → MenuItem`

  构建一个菜单项，用于将所选内容周围的文本块类型更改为给定类型。提供run、active、 和select 属性。其他的必须在 中给出options。options.attrs可以是为文本块节点提供属性的对象。

  要构建您自己的项目，这些图标可能很有用：

-
  `icons: Object`

  一组与编辑器相关的基本图标。包含属性 join, lift, selectParentNode, undo, redo, strong, em, code, link, bulletList, orderedList, 和blockquote，每个属性包含一个可用作 的选项的icon对象 MenuItem。

-
  `
  renderGrouped(view: EditorView, content: readonly readonly MenuElement[][]) → {
    dom: DocumentFragment,
    update: fn(state: EditorState) → boolean
  }
  `

  将给定的（可能是嵌套的）菜单元素数组渲染到文档片段中，在它们之间放置分隔符（并确保当某些组为空时不会出现多余的分隔符）。

## `@tiptap/pm/model`

该模块定义了 ProseMirror 的内容模型，即用于表示和处理文档的数据结构。

### 文件结构
ProseMirror 文档是一棵树。在每个级别，节点 描述内容的类型，并保存 包含其子节点的片段。

#### class Node
此类表示构成 ProseMirror 文档的树中的一个节点。因此，文档是 的实例Node，其子文档也是 的实例Node。

节点是持久数据结构。您无需更改它们，而是使用您想要的内容创建新的。老的一直指着旧的文档形状。通过尽可能多地共享新旧数据之间的结构，可以降低成本，像这样的树形（没有后向指针）使之变得容易。

不要直接改变对象的属性Node。请参阅 指南以获取更多信息。

- `type: NodeType`

  这是节点的类型。

- `attrs: Attrs`

  将属性名称映射到值的对象。允许和需要的属性类型 由节点类型决定。

- `marks: readonly Mark[]`

  应用于此节点的标记（例如是否强调或链接的一部分）。

- `content: Fragment`

  保存节点子节点的容器。

- `text: string`

  对于文本节点，这包含节点的文本内容。

- `nodeSize: number`

  该节点的大小，由基于整数的索引方案定义。对于文本节点，这是字符数。对于其他叶节点来说，就是一个。对于非叶节点，它是内容的大小加二（开始和结束标记）。

- `childCount: number`

  该节点拥有的子节点数量。

- `child(index: number) → Node`

  获取给定索引处的子节点。当索引超出范围时引发错误。

- `maybeChild(index: number) → Node`

  获取给定索引处的子节点（如果存在）。

- `forEach(f: fn(node: Node, offset: number, index: number))`

  调用f每个子节点，传递该节点、其在此父节点中的偏移量及其索引。

-
  `
  nodesBetween(
    from: number,
    to: number,
    f: fn(
      node: Node,
      pos: number,
      parent: Node,
      index: number
    ) → boolean | undefined,
    startPos⁠?: number = 0
  )
  `

  在相对于该节点内容开头的给定两个位置之间递归地调用所有后代节点的回调。使用节点、其相对于原始节点（方法接收者）的位置、其父节点及其子索引来调用回调。当回调对于给定节点返回 false 时，该节点的子节点将不会被递归。最后一个参数可用于指定计数的起始位置。

-
  `
  descendants(
    f: fn(
      node: Node,
      pos: number,
      parent: Node,
      index: number
    ) → boolean | undefined
  )
  `

  为每个后代节点调用给定的回调。当回调返回时，不会下降到节点false。

- `textContent: string`

  连接此片段及其子节点中找到的所有文本节点。

-
  `
  textBetween(
    from: number,
    to: number,
    blockSeparator⁠?: string,
    leafText⁠?: string | fn(leafNode: Node) → string | null
  ) → string
  `

  from获取位置和之间的所有文本to。当 blockSeparator给出时，它将被插入以将文本与不同的块节点分开。如果leafText给出，它将被插入到遇到的每个非文本叶节点，否则 leafText将被使用。

- `firstChild: Node`

  返回此节点的第一个子节点，或者null如果没有子节点则返回。

- `lastChild: Node`

  返回此节点的最后一个子节点，或者null如果没有子节点。

- `eq(other: Node) → boolean`

  测试两个节点是否代表同一篇文档。

- `sameMarkup(other: Node) → boolean`

  将此节点的标记（类型、属性和标记）与另一个节点的标记进行比较。true如果两者具有相同的标记，则返回。

-
  `
  hasMarkup(
    type: NodeType,
    attrs⁠?: Attrs,
    marks⁠?: readonly Mark[]
  ) → boolean
  `

  检查此节点的标记是否对应于给定的类型、属性和标记。

- `copy(content⁠?: Fragment = null) → Node`

  创建一个与此节点具有相同标记的新节点，包含给定的内容（如果没有给出内容，则为空）。

- `mark(marks: readonly Mark[]) → Node`

  使用给定的标记集而不是节点自己的标记创建此节点的副本。

- `cut(from: number, to⁠?: number = this.content.size) → Node`

  创建此节点的副本，仅包含给定位置之间的内容。如果to未给出，则默认为节点末尾。

-
  `
  slice(
    from: number,
    to⁠?: number = this.content.size,
    includeParents⁠?: boolean = false
  ) → Slice
  `

  剪切给定位置之间的文档部分，并将其作为对象返回Slice。

- `replace(from: number, to: number, slice: Slice) → Node`

  将给定位置之间的文档部分替换为给定切片。切片必须“适合”，这意味着其开放边必须能够连接到周围的内容，并且其内容节点必须是它们所在节点的有效子节点。ReplaceError如果违反其中任何一个，则会引发类型错误 。

- `nodeAt(pos: number) → Node`

  查找紧邻给定位置之后的节点。

- `childAfter(pos: number) → {node: Node, index: number, offset: number}`

  查找给定偏移量之后的（直接）子节点（如果有），并将其与其索引和相对于该节点的偏移量一起返回。

- `childBefore(pos: number) → {node: Node, index: number, offset: number}`

  查找给定偏移量之前的（直接）子节点（如果有），并将其与其索引和相对于该节点的偏移量一起返回。

- `resolve(pos: number) → ResolvedPos`

  解析文档中的给定位置，返回一个 包含有关其上下文的信息的对象。

-
  `
  rangeHasMark(
    from: number,
    to: number,
    type: Mark | MarkType
  ) → boolean
  `

  测试给定标记或标记类型是否出现在本文档中两个给定位置之间。

- `isBlock: boolean`

  当这是一个块（非内联节点）时为真

- `isTextblock: boolean`

  当这是一个文本块节点（具有内联内容的块节点）时为 true。

- `inlineContent: boolean`

  当此节点允许内联内容时为 true。

- `isInline: boolean`

  当这是内联节点（文本节点或可以出现在文本中的节点）时为真。

- `isText: boolean`

  当这是文本节点时为真。

- `isLeaf: boolean`

  当这是叶节点时为真。

- `isAtom: boolean`

  当这是一个原子时，即当它没有直接可编辑的内容时，则为真。这通常与 相同，但可以使用 节点规范上的属性isLeaf进行配置（通常在节点显示为不可编辑的节点视图时使用）。atom

- `toString() → string`

  返回此节点的字符串表示形式以用于调试目的。

- `contentMatchAt(index: number) → ContentMatch`

  获取此节点中给定索引处的内容匹配。

-
  `
  canReplace(
    from: number,
    to: number,
    replacement⁠?: Fragment = Fragment.empty,
    start⁠?: number = 0,
    end⁠?: number = replacement.childCount
  ) → boolean
  `

  测试用给定的替换片段（默认为空片段）替换 和 之间的范围（通过子索引）是否会使节点的内容from有效。to您可以选择将start和end索引传递到替换片段中。

-
  `
  canReplaceWith(
    from: number,
    to: number,
    type: NodeType,
    marks⁠?: readonly Mark[]
  ) → boolean
  `

  from测试用给定类型的节点替换范围to（按索引）是否会使节点的内容有效。

- `canAppend(other: Node) → boolean`

  测试给定节点的内容是否可以附加到该节点。如果该节点为空，则仅当两个节点中至少存在一种节点类型时才会返回 true（以避免合并完全不兼容的节点）。

- `check()`

  检查此节点及其后代是否符合架构，如果不符合则引发错误。

- `toJSON() → any`

  返回此节点的 JSON 可序列化表示。

- `static fromJSON(schema: Schema, json: any) → Node`

  从 JSON 表示形式反序列化节点。

#### class Fragment
片段表示节点的子节点的集合。

与节点一样，片段是持久数据结构，您不应改变它们或其内容。相反，您可以在需要时创建新实例。API 试图让这一切变得简单。

- `size: number`

  片段的大小，即其内容节点的大小总和。

-
  `
  nodesBetween(
    from: number,
    to: number,
    f: fn(
      node: Node,
      start: number,
      parent: Node,
      index: number
    ) → boolean | undefined,
    nodeStart⁠?: number = 0,
    parent⁠?: Node
  )
  `

  为给定两个位置（相对于该片段的开头）之间的所有后代节点调用回调。当回调返回时，不会下降到节点false。

-
  `
  descendants(
    f: fn(
      node: Node,
      pos: number,
      parent: Node,
      index: number
    ) → boolean | undefined
  )
  `
  为每个后代节点调用给定的回调。pos将相对于片段的开头。回调可能会返回 false以防止遍历给定节点的子节点。

-
  `
  textBetween(
    from: number,
    to: number,
    blockSeparator⁠?: string,
    leafText⁠?: string | fn(leafNode: Node) → string | null
  ) → string
  `

  from提取和之间的文本to。请参阅 上的相同方法 Node。

- `append(other: Fragment) → Fragment`

  创建一个新片段，其中包含此片段和另一个片段的组合内容。

- `cut(from: number, to⁠?: number = this.size) → Fragment`

  剪切两个给定位置之间的子片段。

- `replaceChild(index: number, node: Node) → Fragment`

  创建一个新片段，其中给定索引处的节点被给定节点替换。

- `addToStart(node: Node) → Fragment`

  通过将给定节点添加到该片段之前来创建一个新片段。

- `addToEnd(node: Node) → Fragment`

  通过将给定节点附加到该片段来创建一个新片段。

- `eq(other: Fragment) → boolean`

  将此片段与另一个片段进行比较。

- `firstChild: Node`

  片段的第一个子级，或者null如果它为空。

- `lastChild: Node`

  片段的最后一个子级，或者null如果它为空。

- `childCount: number`

  该片段中的子节点数。

- `child(index: number) → Node`

  获取给定索引处的子节点。当索引超出范围时引发错误。

- `maybeChild(index: number) → Node`

  获取给定索引处的子节点（如果存在）。

-
  `
  forEach(
    f: fn(node: Node, offset: number, index: number)
  )
  `

  调用f每个子节点，传递该节点、其在此父节点中的偏移量及其索引。

- `findDiffStart(other: Fragment, pos⁠?: number = 0) → number`

  null找到该片段与另一个片段不同或相同的第一个位置。

-
  `
  findDiffEnd(
    other: Fragment,
    pos⁠?: number = this.size,
    otherPos⁠?: number = other.size
  ) → {a: number, b: number}
  `

  从末尾开始搜索，找到该片段与给定片段不同或null相同的第一个位置。由于两个节点中的该位置不相同，因此返回具有两个单独位置的对象。

- `findIndex(pos: number, round⁠?: number = -1) → {index: number, offset: number}`

  查找该片段中给定相对位置对应的索引和内部偏移量。结果对象将在下次调用该函数时重用（覆盖）。（不公开。）

- `toString() → string`

  返回描述该片段的调试字符串。

- `toJSON() → any`

  创建此片段的 JSON 可序列化表示。

- `static fromJSON(schema: Schema, value: any) → Fragment`

  从 JSON 表示中反序列化片段。

- `static fromArray(array: readonly Node[]) → Fragment`

  从节点数组构建片段。确保具有相同标记的相邻文本节点连接在一起。

-
  `
  static from(
  nodes⁠?: Fragment | Node | readonly Node[] | null
  ) → Fragment
  `

  从可以解释为一组节点的事物创建一个片段。对于null，它返回空片段。对于片段来说，就是片段本身。对于节点或节点数组，包含这些节点的片段。

- `static empty: Fragment`

  一个空片段。旨在在节点不包含任何内容时重用（而不是为每个叶节点分配新的空片段）。

#### class Mark
标记是可以附加到节点的一条信息，例如以代码字体或链接强调的节点。它具有一个类型和一组可选的属性，可提供更多信息（例如链接的目标）。标记是通过 a 创建的Schema，它控制存在哪些类型以及它们具有哪些属性。

- `type: MarkType`

  该标记的类型。

- `attrs: Attrs`

  与此标记关联的属性。

- `addToSet(set: readonly Mark[]) → readonly Mark[]`

  给定一组标记，创建一组新的标记，其中也包含该标记，位于正确的位置。如果该标记已在集合中，则返回集合本身。如果存在任何设置为与该标记独占的标记 ，则这些标记将被该标记替换。

- `removeFromSet(set: readonly Mark[]) → readonly Mark[]`

  从给定集合中删除此标记，返回一个新集合。如果该标记不在集合中，则返回集合本身。

- `isInSet(set: readonly Mark[]) → boolean`

  测试该标记是否在给定的标记集中。

- `eq(other: Mark) → boolean`

  测试此标记是否与另一个标记具有相同的类型和属性。

- `toJSON() → any`

  将此标记转换为 JSON 可序列化表示形式。

- `static fromJSON(schema: Schema, json: any) → Mark`

  从 JSON 反序列化标记。

- `static sameSet(a: readonly Mark[], b: readonly Mark[]) → boolean`

  测试两组标记是否相同。

- `static setFrom(marks⁠?: Mark | readonly Mark[] | null) → readonly Mark[]`

  从 null、单个标记或未排序的标记数组创建正确排序的标记集。

- `static none: readonly Mark[]`

  空的标记集。

#### class Slice
切片代表从较大文档中剪下的一部分。它不仅存储片段，还存储两侧节点“打开”（切通）的深度。

-
  `
  new Slice(
    content: Fragment,
    openStart: number,
    openEnd: number
  )
  `

  创建一个切片。当指定非零开放深度时，必须确保片段的适当一侧至少有该深度的节点，即如果片段是空段落节点，并且不能大于openStart1 openEnd。

  开放节点的内容不必符合模式的内容约束，尽管它应该是此类节点的有效开始/结束/中间，具体取决于哪一侧开放。

- `content: Fragment`

  切片的内容。

- `openStart: number`

  片段开始处的开放深度。

- `openEnd: number`

  末端的开放深度。

- `size: number`

  插入到文档中时该切片将添加的大小。

- `eq(other: Slice) → boolean`

  测试此切片是否等于另一个切片。

- `toJSON() → any`

  将切片转换为 JSON 可序列化表示。

- `static fromJSON(schema: Schema, json: any) → Slice`

  从 JSON 表示中反序列化切片。

-
  `
  static maxOpen(
    fragment: Fragment,
    openIsolating⁠?: boolean = true
  ) → Slice
  `

  通过在片段两侧获取最大可能的开放值，从片段创建切片。

- `static empty: Slice`

  空切片。

#### type Attrs
保存节点属性的对象。

#### class ReplaceError extends Error
Node.replace给出无效替换时引发的错误类型。

### 已解决的位置
文档中的位置可以表示为整数 偏移量。但您通常会想要使用更方便的表示形式。

#### class ResolvedPos
您可以解析某个位置以获取有关该位置的更多信息。此类的对象表示这样的已解析位置，提供各种上下文信息和一些辅助方法。

在整个接口中，采用可选depth 参数的方法将把 undefined 解释为this.depth，将负数解释为this.depth + value。

- `depth: number`

  父节点距离根节点的层数。如果该位置直接指向根节点，则为 0。如果它指向顶级段落，则为 1，依此类推。

- `pos: number`

  已解决的位置。

- `parentOffset: number`

  该位置相对于其父节点的偏移量。

- `parent: Node`

  该位置指向的父节点。请注意，即使位置指向文本节点，该节点也不被视为父节点 - 文本节点在此模型中是“平面”的，并且没有内容。

- `doc: Node`

  解析位置的根节点。

- `node(depth⁠?: number) → Node`

  给定级别的祖先节点。p.node(p.depth)与 相同p.parent。

- `index(depth⁠?: number) → number`

  给定级别的祖先的索引。例如，如果指向顶层第二段中的第三个节点，p.index(0)则为 1，p.index(1)为 2。

- `indexAfter(depth⁠?: number) → number`

  在此位置之后指向给定级别的祖先的索引。

- `start(depth⁠?: number) → number`

  给定级别节点起点的（绝对）位置。

- `end(depth⁠?: number) → number`

  给定级别节点末端的（绝对）位置。

- `before(depth⁠?: number) → number`

  给定级别的包裹节点之前的（绝对）位置，或者，当 为 时depth，this.depth + 1为原始位置。

- `after(depth⁠?: number) → number`

  紧邻给定级别的包裹节点之后的（绝对）位置，或当 为 时的原始depth位置this.depth + 1。

- `textOffset: number`

  当此位置指向文本节点时，将返回该位置与文本节点开头之间的距离。对于指向节点之间的位置将为零。

- `nodeAfter: Node`

  直接获取该位置之后的节点（如果有）。如果该位置指向文本节点，则仅返回该位置之后该节点的部分。

- `nodeBefore: Node`

  获取该位置之前的节点（如果有）。如果该位置指向文本节点，则仅返回该位置之前的该节点的部分。

- `posAtIndex(index: number, depth⁠?: number) → number`

  获取给定深度（默认为this.depth）父节点中给定索引处的位置。

- `marks() → readonly Mark[]`

  获取该位置的标记，同时考虑周围标记的inclusive属性。如果该位置位于非空节点的开头，则返回其后面的节点（如果有）的标记。

- `marksAcross($end: ResolvedPos) → readonly Mark[]`

  获取当前位置之后的标记（如果有），但不包含且不存在于位置的标记除外$end。这对于获取删除后要保留的标记集非常有用。null如果该位置位于其父节点的末尾或其父节点不是文本块（在这种情况下不应保留任何标记），则将返回。

- `sharedDepth(pos: number) → number`

  该位置和给定（未解析）位置共享相同父节点的深度。

-
  `
  blockRange(
    other⁠?: ResolvedPos = this,
    pred⁠?: fn(node: Node) → boolean
  ) → NodeRange
  `

  根据此位置和给定位置围绕块内容的分歧位置返回一个范围。例如，如果两者都指向同一个文本块，则将返回该文本块周围的范围。如果它们指向不同的块，则返回其共享祖先中这些块周围的范围。您可以传入一个可选谓词，该谓词将与父节点一起调用，以查看该父节点的范围是否可接受。

- `sameParent(other: ResolvedPos) → boolean`

  查询给定位置是否共享相同的父节点。

- `max(other: ResolvedPos) → ResolvedPos`

  返回此值与给定位置中的较大者。

- `min(other: ResolvedPos) → ResolvedPos`

  返回此值和给定位置中较小的一个。

#### class NodeRange
表示内容的平坦范围，即在同一节点中开始和结束的内容。

-
  `
  new NodeRange(
    $from: ResolvedPos,
    $to: ResolvedPos,
    depth: number
  )
  `

  构建节点范围。$from并且$to应该指向同一个节点，直到至少给定depth，因为节点范围表示单个父节点中的相邻节点集。

- `$from: ResolvedPos`

  沿内容开头的已解决位置。可能具有 depth大于此对象的depth属性，因为这些是用于计算范围的位置，而不是直接在其边界处重新解析的位置。

- `$to: ResolvedPos`

  内容末尾的位置。请参阅 的警告$from。

- `depth: number`

  该范围指向的节点的深度。

- `start: number`

  范围开始处的位置。

- `end: number`

  范围末尾的位置。

- `parent: Node`

  范围指向的父节点。

- `startIndex: number`

  父节点中范围的起始索引。

- `endIndex: number`

  父节点中范围的结束索引。

### 文档架构
每个 ProseMirror 文档都符合一个 schema，该 schema 描述节点集并标记其组成部分，以及这些节点之间的关系，例如哪个节点可能作为其他节点的子节点出现。

**`class Schema<Nodes extends string = any, Marks extends string = any>`**

  文档架构。保存符合文档中可能出现的节点和标记的节点和标记类型对象，并提供创建和反序列化此类文档的功能。

  当给定时，类型参数提供此模式中的节点和标记的名称。

- `new Schema(spec: SchemaSpec<Nodes, Marks>)`

  根据模式规范构造模式。
-
  `
  spec: {
    nodes: OrderedMap<NodeSpec>,
    marks: OrderedMap<MarkSpec>,
    topNode⁠?: string
  }
  `

  模式所基于的规范，并保证其节点和标记属性是OrderedMap实例(而不是原始对象)。

- `nodes: {[name in Nodes]: NodeType} & Object<NodeType>`

  将架构的节点名称映射到节点类型对象的对象。

- `marks: {[name in Marks]: MarkType} & Object<MarkType>`

  从标记名称到标记类型对象的映射。

- `topNodeType: NodeType`

  此架构的默认顶部节点的类型。

- `cached: Object<any>`

  用于存储模块可能想要根据模式计算和缓存的任何值的对象。（如果您想在其中存储某些内容，请尝试使用不太可能发生冲突的属性名称。）

-
  `
  node(
    type: string | NodeType,
    attrs⁠?: Attrs = null,
    content⁠?: Fragment | Node | readonly Node[],
    marks⁠?: readonly Mark[]
  ) → Node
  `

  在此架构中创建一个节点。可以type是字符串或 NodeType实例。属性将使用默认值进行扩展， content可以是 a Fragment、null、 aNode或节点数组。

- `text(text: string, marks⁠?: readonly Mark[]) → Node`

  在架构中创建一个文本节点。不允许使用空文本节点。

- `mark(type: string | MarkType, attrs⁠?: Attrs) → Mark`

  使用给定类型和属性创建标记。

- `nodeFromJSON(json: any) → Node`

  从 JSON 表示形式反序列化节点。这个方法是绑定的。

- `markFromJSON(json: any) → Mark`

  从 JSON 表示中反序列化标记。这个方法是绑定的。


**`interface SchemaSpec<Nodes extends string = any, Marks extends string = any>`**

传递给构造函数的描述架构的对象Schema 。

- `nodes: {[name in Nodes]: NodeSpec} | OrderedMap<NodeSpec>`

  此架构中的节点类型。将名称映射到 NodeSpec描述与该名称关联的节点类型的对象。它们的顺序很重要 - 它决定默认情况下哪些解析规则优先，以及哪些节点在给定 组中排在第一位。

- `marks⁠?: {[name in Marks]: MarkSpec} | OrderedMap<MarkSpec>`

  此架构中存在的标记类型。它们提供的顺序决定了标记集的排序顺序以及尝试解析规则的顺序。

- `topNode⁠?: string`

  架构的默认顶级节点的名称。默认为"doc".

**interface NodeSpec**

定义模式时使用的节点类型的描述。

- `content⁠?: string`

  此节点的内容表达式，如架构指南中所述。如果未给出，则该节点不允许任何内容。

- `marks⁠?: string`

  该节点内部允许的标记。可以是引用标记名称或组的以空格分隔的字符串，"_" 以显式允许所有标记或""禁止标记。如果未给出，则具有内联内容的节点默认允许所有标记，其他节点默认不允许标记。

- `group⁠?: string`

  该节点所属的组或以空格分隔的组，可以在架构的内容表达式中引用。

- `inline⁠?: boolean`

  对于内联节点应设置为 true。（对于文本节点隐含。）

- `atom⁠?: boolean`

  可以设置为 true 来指示，虽然这不是叶节点，但它没有直接可编辑的内容，应被视为视图中的单个单元。

- `attrs⁠?: Object<AttributeSpec>`

  该类型的节点获取的属性。

- `selectable⁠?: boolean`

  控制是否可以选择此类型的节点作为节点选择。对于非文本节点默认为 true。

- `draggable⁠?: boolean`

  确定是否可以在不选择该类型的节点的情况下拖动该节点。默认为 false。

- `code⁠?: boolean`

  可用于指示该节点包含代码，这会导致某些命令的行为不同。

- `whitespace⁠?: "pre" | "normal"`

  控制解析该节点中的空白的方式。默认值为 "normal"，这会导致DOM 解析器在正常模式下折叠空白，否则对其进行规范化（用空格替换换行符等）。"pre"使解析器保留节点内的空格。当未给出此选项但code为 true 时，whitespace 将默认为"pre". 请注意，此选项不会影响节点的渲染方式 - 这应该由toDOM 和/或样式处理。

- `definingAsContext⁠?: boolean`

  确定在替换操作（例如粘贴）期间该节点是否被视为重要的父节点。非定义（默认）节点在其全部内容被替换时会被删除，而定义节点则保留并包装插入的内容。

- `definingForContent⁠?: boolean`

  在插入的内容中，尽可能保留内容的定义父项。通常，非默认段落文本块类型以及可能的列表项被标记为定义。

- `defining⁠?: boolean`

  启用后，同时启用 definingAsContext和 definingForContent。

- `isolating⁠?: boolean`

  启用后（默认值为 false），此类型节点的侧面将视为常规编辑操作（如退格或提升）不会跨越的边界。可能应该启用此功能的节点的一个示例是表格单元格。

- `toDOM⁠?: fn(node: Node) → DOMOutputSpec`

  定义将此类型的节点序列化为 DOM/HTML 的默认方式（如 所使用的 DOMSerializer.fromSchema）。应该返回一个 DOM 节点或描述一个的数组结构，其中有一个可选的数字零（“洞”）来指示节点内容应插入的位置。

  对于文本节点，默认是创建一个文本DOM节点。尽管可以创建一个以不同方式呈现文本的序列化器，但这在编辑器内部不受支持，因此您不应在文本节点规范中覆盖它。

- `parseDOM⁠?: readonly ParseRule[]`

  将 DOM 解析器信息与此节点相关联，可用于DOMParser.fromSchema自动派生解析器。规则中的字段node是隐含的（该节点的名称将自动填写）。如果您提供自己的解析器，则无需在模式中指定解析规则。

- `toDebugString⁠?: fn(node: Node) → string`

  定义将此类型的节点序列化为字符串表示形式以进行调试的默认方式（例如，在错误消息中）。

- `leafText⁠?: fn(node: Node) → string`

  定义将此类型的叶节点序列化为字符串的 默认方式（如Node.textBetween和 所使用Node.textContent）。

- `[string]: any`

  节点规范可能包括可由其他代码通过 读取的任意属性NodeType.spec。

**interface MarkSpec**

用于在创建模式时定义标记。

- `attrs⁠?: Object<AttributeSpec>`

  该类型标记所获得的属性。

- `inclusive⁠?: boolean`

  当光标位于其末尾时（或位于其开头，当光标也是父节点的开头时），此标记是否应处于活动状态。默认为 true。

- `excludes⁠?: string`

  确定该标记可以与哪些其他标记共存。应该是一个以空格分隔的字符串，用于命名其他标记或标记组。将标记添加到集合后，它排除的所有标记都会在此过程中删除。如果该集合包含排除新标记但其本身未被新标记排除的任何标记，则该标记不能添加到该集合中。您可以使用该值"_"来指示该标记排除架构中的所有标记。

  默认为仅与相同类型的标记互斥。您可以将其设置为空字符串（或任何不包含标记本身名称的字符串），以允许给定类型的多个标记共存（只要它们具有不同的属性）。

- `group⁠?: string`

  该标记所属的一个或多个以空格分隔的组。

- `spanning⁠?: boolean`

  确定该类型的标记在序列化为 DOM/HTML 时是否可以跨越多个相邻节点。默认为 true。

- `toDOM⁠?: fn(mark: Mark, inline: boolean) → DOMOutputSpec`

  定义此类型标记应序列化为 DOM/HTML 的默认方式。当生成的规范包含一个洞时，那就是放置标记内容的地方。否则，它将附加到顶部节点。

- `parseDOM⁠?: readonly ParseRule[]`

  将 DOM 解析器信息与此标记相关联（请参阅相应的节点规范字段）。mark规则中的字段是隐含的。

- `[string]: any`

  MarkType.spec标记规格可以包括在使用标记时可以检查的附加属性。

**interface AttributeSpec**

用于定义节点或标记的属性。

- `default⁠?: any`

  此属性的默认值，在未提供显式值时使用。每当创建具有默认值的类型的节点或标记时，都必须提供没有默认值的属性。

**class NodeType**

节点类型是每个分配一次Schema并用于 标记 Node实例的对象。它们包含有关节点类型的信息，例如其名称及其代表的节点类型。

- `name: string`

  节点类型在此模式中的名称。

- `schema: Schema`

  Schema返回节点类型所属的链接。

- `spec: NodeSpec`

  该类型所基于的规范

- `inlineContent: boolean`

  如果此节点类型具有内联内容，则为 true。

- `isBlock: boolean`

  如果这是块类型则为 True

- `isText: boolean`

  如果这是文本节点类型，则为 true。

- `isInline: boolean`

  如果这是内联类型，则为 True。

- `isTextblock: boolean`

  如果这是文本块类型（包含内联内容的块），则为 True。

- `isLeaf: boolean`

  对于不允许任何内容的节点类型为真。

- `isAtom: boolean`

  当此节点是原子时，即当它没有直接可编辑的内容时，为真。

- `contentMatch: ContentMatch`

  节点类型的内容表达式的起始匹配。

- `markSet: readonly MarkType[]`

  该节点中允许的标记集。null表示允许所有标记。

- `whitespace: "pre" | "normal"`

  节点类型的空白选项。

- `hasRequiredAttrs() → boolean`

  告诉您此节点类型是否具有任何必需的属性。

- `compatibleContent(other: NodeType) → boolean`

  指示该节点是否允许某些与给定节点类型相同的内容。

-
  `
  create(
    attrs⁠?: Attrs = null,
    content⁠?: Fragment | Node | readonly Node[] | null,
    marks⁠?: readonly Mark[]
  ) → Node
  `

  创建一个Node这种类型的。null给定的属性被检查并默认（如果不存在必需的属性，您可以传递以完全使用类型的默认值）。content 可以是Fragment、节点、节点数组或 null。类似地，marks可以null默认为空标记集。

-
  `
  createChecked(
    attrs⁠?: Attrs = null,
    content⁠?: Fragment | Node | readonly Node[] | null,
    marks⁠?: readonly Mark[]
  ) → Node
  `

  与create类似，但是根据节点类型的内容限制检查给定的内容，如果不匹配则抛出错误。

-
  `
  createAndFill(
    attrs⁠?: Attrs = null,
    content⁠?: Fragment | Node | readonly Node[] | null,
    marks⁠?: readonly Mark[]
  ) → Node
  `

  就像create一样，但看看是否有必要在给定片段的开始或结束处添加节点以使其适合节点。如果找不到合适的包装，则返回null。请注意，由于总是可以创建所需的节点，如果您传递null或Fragment，这将总是成功的。空为内容。

- `validContent(content: Fragment) → boolean`

  如果给定片段是具有给定属性的此节点类型的有效内容，则返回 true。

- `allowsMarkType(markType: MarkType) → boolean`

  检查该节点是否允许给定的标记类型。

- `allowsMarks(marks: readonly Mark[]) → boolean`

  测试该节点是否允许给定的标记集。

- `allowedMarks(marks: readonly Mark[]) → readonly Mark[]`

  从给定集中删除此节点中不允许的标记。

**class MarkType**

与节点一样，标记（与节点关联以表示强调或作为链接的一部分）是 用类型对象标记的，每个对象实例化一次Schema。

- `name: string`

  标记类型的名称。

- `schema: Schema`

  此标记类型实例所属的架构。

- `spec: MarkSpec`

  该类型所基于的规范。

- `create(attrs⁠?: Attrs = null) → Mark`

  创建此类型的标记。attrs可以是null或仅包含标记的某些属性的对象。其他的，如果有默认值，将被添加。

- `removeFromSet(set: readonly Mark[]) → readonly Mark[]`

  当给定集合中存在这种类型的标记时，返回没有它的新集合。否则，返回输入集。

- `isInSet(set: readonly Mark[]) → Mark`

  测试给定集合中是否存在该类型的标记。

- `excludes(other: MarkType) → boolean`

  查询给定标记类型是否被 该标记类型排除。

**class ContentMatch**

此类的实例表示节点类型的 内容表达式的匹配状态，可用于查找此处是否还有其他内容匹配，以及给定位置是否是节点的有效末尾。

- `validEnd: boolean`

  当此匹配状态表示节点的有效结束时为 True。

- `matchType(type: NodeType) → ContentMatch`

  匹配节点类型，如果成功则返回该节点之后的匹配项。

-
  `
  matchFragment(
    frag: Fragment,
    start⁠?: number = 0,
    end⁠?: number = frag.childCount
  ) → ContentMatch
  `

  尝试匹配一个片段。成功时返回结果匹配。

- `defaultType: NodeType`

  获取该匹配位置可以生成的第一个匹配节点类型。

-
  `
  fillBefore(
    after: Fragment,
    toEnd⁠?: boolean = false,
    startIndex⁠?: number = 0
  ) → Fragment
  `

  尝试匹配给定的片段，如果失败，看看是否可以通过在其前面插入节点来使其匹配。成功后，返回插入节点的片段（如果不需要插入任何内容，则该片段可能为空）。当toEnd为 true 时，仅当结果匹配到达内容表达式的末尾时才返回片段。

- `findWrapping(target: NodeType) → readonly NodeType[]`

  找到一组允许给定类型的节点出现在该位置的包装节点类型。结果可能为空（当它直接适合时），并且当不存在此类包装时将为 null。

- `edgeCount: number`

  该节点在描述内容表达式的有限自动机中具有的传出边的数量。

- `edge(n: number) → {type: NodeType, next: ContentMatch}`

  获取描述内容表达式的有限自动机中从此节点起的第 `_n_​`个出边。

### DOM处理
由于将文档表示为 DOM 节点树是 ProseMirror 操作方式的核心，因此 DOM解析和 序列化与模型集成在一起。

（但请注意，您不需要加载DOM 实现即可使用此模块。）

**class DOMParser**

DOM 解析器表示将 DOM 内容解析为符合给定模式的 ProseMirror 文档的策略。它的行为是由一系列规则定义的。

-
  `
  new DOMParser(
    schema: Schema,
    rules: readonly ParseRule[]
  )
  `

  使用给定的解析规则创建一个针对给定模式的解析器。

- `schema: Schema`

  解析器解析的模式。

- `rules: readonly ParseRule[]`

  解析器使用的解析规则集（按优先顺序排列）。

- `parse(dom: DOMNode, options⁠?: ParseOptions = {}) → Node`

  从 DOM 节点的内容解析文档。

- `parseSlice(dom: DOMNode, options⁠?: ParseOptions = {}) → Slice`

  解析给定 DOM 节点的内容，例如 parse，并采用相同的选项集。但与生成整个节点的该方法不同，该方法返回一个在两侧打开的切片，这意味着模式约束不适用于输入左侧节点的开头和输入左侧节点的结尾。结尾。

- `static fromSchema(schema: Schema) → DOMParser`

  使用模式节点规范中列出的解析规则构造 DOM 解析器，并按 优先级重新排序。

**interface ParseOptions**

parse这些是和 方法识别的选项 parseSlice。

- `preserveWhitespace⁠?: boolean | "full"`

  默认情况下，空白会按照 HTML 的规则折叠。传递 true以保留空白，但将换行符标准化为空格，并"full"完全保留空白。

- `findPositions⁠?: {node: DOMNode, offset: number, pos⁠?: number}[]`

  当给出时，解析器除了解析内容之外，还将记录给定 DOM 位置的文档位置。它将通过写入对象、添加pos保存文档位置的属性来实现这一点。不在解析内容中的 DOM 位置将不会被写入。

- `from⁠?: number`

  开始解析的子节点索引。

- `to⁠?: number`

  停止解析的子节点索引。

- `topNode⁠?: Node`

  默认情况下，内容被解析为模式的默认 顶部节点类型。您可以传递此选项以使用来自不同节点的类型和属性作为顶部容器。

- `topMatch⁠?: ContentMatch`

  提供解析到顶部节点的内容所匹配的起始内容匹配。

- `context⁠?: ResolvedPos`

  解析时在给定顶部节点之上的一组附加节点，将其计为上下文 。

**interface ParseRule**

描述如何将给定 DOM 节点或内联样式解析为 ProseMirror 节点或标记的值。

- `tag⁠?: string`

  描述要匹配的 DOM 元素类型的 CSS 选择器。单个规则应该具有atag或 a属性style。

- `namespace⁠?: string`

  要匹配的命名空间。这应该与 一起使用tag。仅当命名空间匹配或此属性为空时，节点才会匹配。

- `style⁠?: string`

  要匹配的 CSS 属性名称。给出后，此规则与列出该属性的内联样式匹配。也可以采用 形式 "property=value"，在这种情况下，仅当属性的值与给定值完全匹配时规则才匹配。（对于更复杂的过滤器，使用getAttrs 并返回 false 来指示匹配失败。）匹配样式的规则可能只产生标记，而不产生节点。

- `priority⁠?: number`

  可用于更改模式中尝试解析规则的顺序。优先级较高的优先。没有优先级的规则被视为优先级为 50。此属性仅在模式中有意义 - 当直接构造解析器时，将使用规则数组的顺序。

- `consuming⁠?: boolean`

  默认情况下，当规则与元素或样式匹配时，没有其他规则有机会匹配它。通过将其设置为false，您表明即使此规则匹配，它后面的其他规则也应该运行。

- `context⁠?: string`

  如果给出，则限制此规则仅在当前上下文（内容被解析到的父节点）与此表达式匹配时才匹配。应包含一个或多个节点名称或节点组名称，后跟单斜杠或双斜杠。例如，"paragraph/"意味着规则仅在父节点是段落时匹配，"blockquote/paragraph/"将其限制在块引用内的段落中，并 "section//"匹配节内的任何位置 - 双斜杠匹配祖先节点的任何序列。为了允许多个不同的上下文，它们可以用竖线 ( |) 字符分隔，如"blockquote/|list_item/"。

- `node⁠?: string`

  当此规则匹配时要创建的节点类型的名称。仅对具有属性的规则有效tag，对样式规则无效。node每个规则应具有 a 、mark、clearMark或 属性之一ignore（除非它出现在 节点或标记规范中，在这种情况下node或 mark属性将从其位置派生）。

- `mark⁠?: string`

  用于包装匹配内容的标记类型的名称。

- `clearMark⁠?: fn(mark: Mark) → boolean`

  样式规则可以从活动标记集中删除标记。

- `ignore⁠?: boolean`

  如果为 true，则忽略与此规则匹配的内容。

- `closeParent⁠?: boolean`

  当 true 时，找到匹配此规则的元素将关闭当前节点。

- `skip⁠?: boolean`

  如果为 true，则忽略与此规则匹配的节点，但解析其内容。

- `attrs⁠?: Attrs`

  此规则创建的节点或标记的属性。当 getAttrs提供时，它优先。

- `getAttrs⁠?: fn(node: HTMLElement | string) → false | Attrs`

  用于计算由该规则创建的节点或标记的属性的函数。还可用于描述 DOM 元素或样式必须匹配的进一步条件。当它返回时 false，规则将不匹配。当它返回 null 或未定义时，将被解释为空/默认属性集。

  对于规则，使用 DOM 元素进行调用tag；对于规则，使用字符串（样式的值）进行调用style。

- `contentElement⁠?: string | HTMLElement | fn(node: DOMNode) → HTMLElement`

  对于tag生成非叶节点或标记的规则，默认情况下 DOM 元素的内容被解析为标记或节点的内容。如果子节点位于后代节点中，则这可能是解析器必须用来查找实际内容元素的 CSS 选择器字符串，或者是向解析器返回实际内容元素的函数。

- `getContent⁠?: fn(node: DOMNode, schema: Schema) → Fragment`

  可用于覆盖匹配节点的内容。如果存在，则使用此函数的结果，而不是解析节点的子节点。

- `preserveWhitespace⁠?: boolean | "full"`

  控制在解析匹配元素内的内容时是否应保留空格。false意味着空白可能会折叠，true意味着应该保留空白但换行符标准化为空格，并且"full"意味着换行符也应该被保留。

**class DOMSerializer**

DOM 序列化器知道如何将 ProseMirror 节点和各种类型的标记转换为 DOM 节点。

-
  `
  new DOMSerializer(
    nodes: Object<fn(node: Node) → DOMOutputSpec>,
    marks: Object<
      fn(mark: Mark, inline: boolean) → DOMOutputSpec
    >
  )
  `

  创建一个序列化器。nodes应该将节点名称映射到采用节点并返回相应 DOM 描述的函数。marks对标记名称执行相同的操作，但还会获取一个参数，告诉它标记的内容是块内容还是内联内容（对于典型用途，它将始终是内联内容）。标记序列化器可以null指示该类型的标记不应被序列化。

- `nodes: Object<fn(node: Node) → DOMOutputSpec>`

  节点序列化函数。

-
  `
  marks: Object<fn(mark: Mark, inline: boolean) → DOMOutputSpec>
  `

  标记序列化函数。

-
  `
  serializeFragment(
    fragment: Fragment,
    options⁠?: {document⁠?: Document} = {},
    target⁠?: HTMLElement | DocumentFragment
  ) → HTMLElement | DocumentFragment
  `

  将此片段的内容序列化为 DOM 片段。当不在浏览器中时，document应传递包含 DOM 文档的选项，以便序列化器可以创建节点。

-
  `
  serializeNode(
    node: Node,
    options⁠?: {document⁠?: Document} = {}
  ) → DOMNode
  `

  将此节点序列化为 DOM 节点。当您需要序列化文档的一部分而不是整个文档时，这会很有用。要序列化整个文档，请使用 serializeFragment其content。

-
  `
  static renderSpec(
    doc: Document,
    structure: DOMOutputSpec,
    xmlNS⁠?: string = null
  ) → {dom: DOMNode, contentDOM⁠?: HTMLElement}
  `

  将输出规范渲染到 DOM 节点。如果规格中有孔（零），contentDOM则将指向有孔的节点。

- `static fromSchema(schema: Schema) → DOMSerializer`

  使用toDOM 模式节点和标记规范中的属性构建序列化器。

- `static nodesFromSchema(schema: Schema) → Object<fn(node: Node) → DOMOutputSpec>`

  将模式节点规范中的序列化器收集到一个对象中。这可以作为构建自定义序列化器的基础。

- `static marksFromSchema(schema: Schema) → Object<fn(mark: Mark, inline: boolean) → DOMOutputSpec>`

  将模式标记规范中的序列化器收集到一个对象中。

- `type DOMOutputSpec = string | DOMNode | {dom: DOMNode, contentDOM⁠?: HTMLElement} | [string, any]`

  DOM 结构的描述。可以是字符串（被解释为文本节点）、DOM 节点（被解释为自身）、对象{dom, contentDOM}或数组。

  数组描述了一个 DOM 元素。数组中的第一个值应该是一个字符串 - DOM 元素的名称，可以选择以名称空间 URL 和空格作为前缀。如果第二个元素是普通对象，则它被解释为该元素的一组属性。此后的任何元素（如果不是属性对象，则包括第二个元素）都被解释为 DOM 元素的子元素，并且必须是有效值DOMOutputSpec或数字零。

  数字零（发音为“hole”）用于指示应插入节点的子节点的位置。如果它出现在输出规范中，它应该是其父节点中的唯一子元素。

## `@tiptap/pm/schema-basic`
该模块定义了一个简单的模式。您可以直接使用它、扩展它，或者只是挑选一些节点并标记规格以在新模式中使用。

```
schema: Schema<
  "blockquote" |
  "doc" |
  "paragraph" |
  "horizontal_rule" |
  "heading" |
  "code_block" |
  "text" |
  "image" |
  "hard_break"
  ,
  "code" | "em" | "strong" | "link"
>
```

此模式大致对应于CommonMark使用的文档模式 ，减去模块中定义的列表元素`prosemirror-schema-list` 。

要重用此架构中的元素，请扩展或读取其 spec.nodes和spec.marks 属性。

- `nodes: Object`

  此架构中定义的节点的规范。

- `doc: NodeSpec`

  NodeSpec 顶级文档节点。

- `paragraph: NodeSpec`

  纯段落文本块。在 DOM 中表示为`<p>`元素。

- `blockquote: NodeSpec`

  包裹一个或多个块的块引用(`<blockquote>`)。

- `horizontal_rule: NodeSpec`

  水平线（`<hr>`）。

- `heading: NodeSpec`

  标题文本块，其level属性应包含数字 1 到 6。针对元素进行解析和序列`<h1>`化 `<h6>`。

- `code_block: NodeSpec`

  代码清单。默认情况下不允许标记或非文本内联节点。表示为一个内部`<pre>`有一个元素的元素`<code>`

- `text: NodeSpec`

  文本节点。

- `image: NodeSpec`

  内联图像 (`<img>`) 节点。支持src、 alt、 和href属性。后两者默认为空字符串。

- `hard_break: NodeSpec`

  硬换行符，在 DOM 中表示为`<br>`。

- `marks: Object`

  架构中标记的规范。

- `link: MarkSpec`

  一条链接。具有href和title属性。title 默认为空字符串。作为`<a>` 元素进行渲染和解析。

- `em: MarkSpec`

  强调标记。渲染为`<em>`元素。`<i>`具有也匹配和的解析规则`font-style: italic`。

- `strong: MarkSpec`

  一个强有力的标记。呈现为`<strong>`，解析规则也匹配 `<b>`和`font-weight: bold`。

- `code: MarkSpec`

  代码字体标记。表示为`<code>`元素。

## `@tiptap/pm/schema-list`

该模块导出与列表相关的架构元素和命令。这些命令假定列表是可嵌套的，但限制是列表项的第一个子项是纯段落。

这些是节点规格：

- `orderedList: NodeSpec`

  有序列表节点规范。有一个属性 ，order它确定列表开始计数的数字，默认为 1。表示为元素`<ol>`。

- `bulletList: NodeSpec`

  项目符号列表节点规范，在 DOM 中表示为`<ul>`.

- `listItem: NodeSpec`

  列表项 (`<li>`) 规格。

-
  `
  addListNodes(
    nodes: OrderedMap<NodeSpec>,
    itemContent: string,
    listGroup⁠?: string
  ) → OrderedMap<NodeSpec>
  `

  用于将与列表相关的节点类型添加到指定模式节点的映射的便捷函数。添加 `orderedList as "ordered_list"`、 `bulletList as "bullet_list"`和 `listItem as "list_item"`。

  itemContent确定列表项的内容表达式。如果您希望此模块中定义的命令应用于您的列表结构，它应该具有类似`"paragraph block*"`或 的 形状`"paragraph (ordered_list | bullet_list)*"`。listGroup可以给列表节点类型分配一个组名，例如 `"block"`.

使用它看起来像这样：

```
const mySchema = new Schema({
  nodes: addListNodes(baseSchema.spec.nodes, "paragraph block*", "block"),
  marks: baseSchema.spec.marks
})
```

以下功能是命令：

- `wrapInList(listType: NodeType, attrs⁠?: Attrs = null) → Command`

  返回一个命令函数，该函数将选择内容包装在具有给定类型和属性的列表中。如果dispatch为 null，则仅返回一个值来指示这是否可能，但不实际执行更改。

- `splitListItem(itemType: NodeType, itemAttrs⁠?: Attrs) → Command`

  构建一个命令，通过同时拆分列表项来拆分列表项顶层的非空文本块。

- `liftListItem(itemType: NodeType) → Command`

  创建一个命令以将所选内容周围的列表项提升到环绕列表中。

- `sinkListItem(itemType: NodeType) → Command`

  创建一个命令将所选内容周围的列表项下沉到内部列表中。

## `@tiptap/pm/state`

该模块实现 ProseMirror 编辑器的状态对象，以及选择的表示和插件抽象。

### 编辑器状态
ProseMirror 将所有编辑器状态（基本上，创建与当前编辑器一样的编辑器所需的内容）保留在单个 对象中。通过向该对象应用事务来更新该对象（创建新状态） 。

**class EditorState**

ProseMirror 编辑器的状态由该类型的对象表示。状态是一种持久数据结构 - 它不会更新，而是使用该apply方法从旧状态值计算出新状态值。

一个状态拥有许多内置字段，插件可以 定义其他字段。

- `doc: Node`

  当前文档。

- `selection: Selection`

  选择。

- `storedMarks: readonly Mark[]`

  应用于下一个输入的一组标记。当没有设置显式标记时将为空。

- `schema: Schema`

  国家文件的架构。

- `plugins: readonly Plugin[]`

  在此状态下处于活动状态的插件。

- `apply(tr: Transaction) → EditorState`

  应用给定的事务来产生新的状态。

- `applyTransaction(rootTr: Transaction) → {state: EditorState, transactions: readonly Transaction[]}`

  它的详细变体apply返回所应用的精确交易（这可能受到插件交易挂钩的影响）以及新状态。

- `tr: Transaction`

  从此状态开始事务。

- `reconfigure(config: Object) → EditorState`

  基于此创建一个新状态，但使用一组经过调整的活动插件。两组插件中存在的状态字段保持不变。那些不再存在的被删除，而那些新的则使用它们的 init方法进行初始化，传入新的配置对象。

  **config**

    - `plugins⁠?: readonly Plugin[]`
    新的活动插件集。

- `toJSON(pluginFields⁠?: Object<Plugin>) → any`

将此状态序列化为 JSON。如果要序列化插件的状态，请将要在生成的 JSON 对象中使用的对象映射属性名称传递给插件对象。该参数也可以是字符串或数字，在这种情况下它会被忽略，以支持JSON.stringify调用toString方法的方式。

- `static create(config: EditorStateConfig) → EditorState`

  创建一个新的状态。

-
  `
  static fromJSON(
    config: Object,
    json: any,
    pluginFields⁠?: Object<Plugin>
  ) → EditorState
  `

  反序列化状态的 JSON 表示形式。config应该至少有一个schema字段，并且应该包含用于初始化状态的插件数组。pluginFields可用于反序列化插件的状态，方法是将插件实例与它们在 JSON 对象中使用的属性名称相关联。

  **config**

  - schema: Schema
    要使用的架构。
  - plugins⁠?: readonly Plugin[]
    活动插件集。

#### interface EditorStateConfig
传递给EditorState.create的对象类型。

- `schema⁠?: Schema`

  要使用的模式（仅在未doc指定时才相关）。

- `doc⁠?: Node`

  起始文档。schema 必须提供此或此。

- `selection⁠?: Selection`

  文档中的有效选择。

- `storedMarks⁠?: readonly Mark[]`

  初始存储标记集。

- `plugins⁠?: readonly Plugin[]`

  在此状态下应处于活动状态的插件。

#### class Transaction extends Transform
编辑器状态事务，可应用于状态以创建更新的状态。用于 EditorState.tr创建实例。

事务跟踪文档的更改（它们是 的子类 ），而且还跟踪其他状态更改，例如选择更新和存储标记Transform集的调整。此外，您可以在事务中存储元数据属性，这些属性是客户端代码或插件可以用来描述事务所代表的内容的额外信息，以便它们可以相应地更新自己的状态。

编辑器视图使用一些元数据属性：它将一个"pointer"带有值的 属性附加true到由鼠标或触摸输入直接引起的选择事务，一个"composition"包含 ID 的属性，该 ID 标识导致它到由组合 DOM 输入引起的事务的组合，以及一个"uiEvent"属性其中可能是"paste"、 "cut"、 或"drop"。

- `time: number`

  与此事务关联的时间戳，格式与 相同Date.now()。

- `storedMarks: readonly Mark[]`

  此交易设置的存储标记（如果有）。

- `selection: Selection`

  交易的当前选择。这默认为通过事务中的步骤 映射的编辑器选择，但可以使用 覆盖setSelection。

- `setSelection(selection: Selection) → Transaction`

  更新交易的当前选择。将确定应用交易时编辑器获得的选择。

- `selectionSet: boolean`

  此事务是否显式更新了选择。

- `setStoredMarks(marks: readonly Mark[]) → Transaction`

  设置当前存储的标记。

- `ensureMarks(marks: readonly Mark[]) → Transaction`

  确保当前存储的标记，或者如果为空，则确保选择的标记与给定的标记集匹配。如果情况已经如此，则不执行任何操作。

- `addStoredMark(mark: Mark) → Transaction`

  将标记添加到存储的标记集中。

- `removeStoredMark(mark: Mark | MarkType) → Transaction`

  从存储的标记集中删除标记或标记类型。

- `storedMarksSet: boolean`

  是否为该事务显式设置了存储的标记。

- `setTime(time: number) → Transaction`

  更新交易的时间戳。

- `replaceSelection(slice: Slice) → Transaction`

  将当前选择替换为给定切片。

- `replaceSelectionWith(node: Node, inheritMarks⁠?: boolean = true) → Transaction`

  将选择替换为给定节点。当inheritMarks为 true 并且内容是内联时，它会继承插入位置的标记。

- `deleteSelection() → Transaction`

  删除选择。

- `insertText(text: string, from⁠?: number, to⁠?: number) → Transaction`

  使用包含给定字符串的文本节点替换给定范围或选择（如果未给出范围）。

-
  `
  setMeta(
    key: string | Plugin | PluginKey,
    value: any
  ) → Transaction
  `

  在此事务中存储元数据属性，按名称或插件键入。

- `getMeta(key: string | Plugin | PluginKey) → any`

  检索给定名称或插件的元数据属性。

- `isGeneric: boolean`

  如果此事务不包含任何元数据，则返回 true，因此可以安全地扩展。

- `scrollIntoView() → Transaction`

  指示编辑器在更新到此事务生成的状态时应将选择滚动到视图中。

- `scrolledIntoView: boolean`

  当此交易已scrollIntoView调用时为真。

-
  `
  type Command = fn(
    state: EditorState,
    dispatch⁠?: fn(tr: Transaction),
    view⁠?: EditorView
  ) → boolean
  `

  命令是接受状态和可选事务调度函数的函数，并且......
  - 确定它们是否适用于此状态
  - 如果没有则返回 false
  - 如果传递了分派，则可能通过将事务传递给分派来执行它们的效果
  - 返回 true

在某些情况下，编辑器视图作为第三个参数传递。

### 选择
ProseMirror 选择可以是多种类型之一。该模块定义了经典文本选择 （其中光标是一种特殊情况）和节点 选择（其中选择特定文档节点）的类型。可以使用自定义选择类型来扩展编辑器。

#### abstract class Selection
编辑器选择的超类。每个选择类型都应该扩展这一点。不应直接实例化。

-
  `
  new Selection(
    $anchor: ResolvedPos,
    $head: ResolvedPos,
    ranges⁠?: readonly SelectionRange[]
  )
  `

  使用头部、锚点和范围初始化选择。如果未给出范围，则构建跨$anchor和 的 单个范围$head。

- `$anchor: ResolvedPos`

  已解析的选区锚点（修改选区时保留在原处的一侧）。

- `$head: ResolvedPos`

  已解析的选区头部（修改选区时移动的一侧）。

- `ranges: readonly SelectionRange[]`

  选择涵盖的范围。

- `anchor: number`

  选择的锚点，作为未解决的位置。

- `head: number`

  选择的头。

- `from: number`

  选择的主要范围的下限。

- `to: number`

  选择的主要范围的上限。

- `$from: ResolvedPos`

  选择的主要范围的已解析下限。

- `$to: ResolvedPos`

  选择的主要范围的已解析上限。

- `empty: boolean`

  指示所选内容是否包含任何内容。

- `abstract eq(selection: Selection) → boolean`

  测试该选择是否与另一个选择相同。

- `abstract map(doc: Node, mapping: Mappable) → Selection`

  通过可映射的事物来映射此选择 。doc应该是我们要映射到的新文档。

- `content() → Slice`

  获取此选择的内容作为切片。

-
  `
  replace(
    tr: Transaction,
    content⁠?: Slice = Slice.empty
  )
  `

  用切片替换所选内容，或者，如果未给出切片，则删除所选内容。将附加到给定的交易。

- `replaceWith(tr: Transaction, node: Node)`

  将选择替换为给定节点，将更改附加到给定事务。

- `abstract toJSON() → any`

  将选择转换为 JSON 表示形式。当为自定义选择类实现此操作时，请确保为对象提供一个 属性，该属性的值与您注册type类时使用的 ID 相匹配 。

- `getBookmark() → SelectionBookmark`

  获取此选择的书签，该书签是无需访问当前文档即可映射的值，稍后再次解析为给定文档的真实选择。（这主要由历史记录用来跟踪和恢复旧的选择。）此方法的默认实现只是将选择转换为文本选择并返回其书签。

- `visible: boolean`

  控制当此类型的选择在浏览器中处于活动状态时，所选范围是否应对用户可见。默认为true.

-
  `
  static findFrom(
    $pos: ResolvedPos,
    dir: number,
    textOnly⁠?: boolean = false
  ) → Selection
  `

  dir从给定位置开始查找有效的光标或叶节点选择，如果为负则向后搜索，如果为正则向前搜索。当textOnly为 true 时，仅考虑光标选择。当没有找到有效的选择位置时将返回 null。

- `static near($pos: ResolvedPos, bias⁠?: number = 1) → Selection`

  在给定位置附近查找有效的光标或叶节点选择。默认先向前搜索，如果bias为负数则先向后搜索。

- `static atStart(doc: Node) → Selection`

  查找最接近给定文档开头的光标或叶节点选择。AllSelection如果不存在有效位置，将返回一个 。

- `static atEnd(doc: Node) → Selection`

  查找最接近给定文档末尾的光标或叶节点选择。

- `static fromJSON(doc: Node, json: any) → Selection`

  反序列化所选内容的 JSON 表示形式。必须为自定义类实现（作为静态类方法）。

-
  `
  static jsonID(
    id: string,
    selectionClass: {fromJSON: fn(doc: Node, json: any) → Selection}
  ) → {fromJSON: fn(doc: Node, json: any) → Selection}
  `

  为了能够反序列化 JSON 中的选择，自定义选择类必须使用 ID 字符串注册自身，以便可以消除歧义。尝试选择不太可能与其他模块中的类发生冲突的内容。

#### class TextSelection extends Selection
文本选择代表经典的编辑器选择，具有头部（移动侧）和锚点（固定侧），两者都指向文本块节点。它可以为空（常规光标位置）。

-
  `
  new TextSelection(
    $anchor: ResolvedPos,
    $head⁠?: ResolvedPos = $anchor
  )
  `

  在给定点之间构建文本选择。

- `$cursor: ResolvedPos`

  如果这是光标选择（空文本选择），则返回解析位置，否则返回 null。

-
  `
  static create(
    doc: Node,
    anchor: number,
    head⁠?: number = anchor
  ) → TextSelection
  `

  从未解析的位置创建文本选择。

-
  `
  static between(
    $anchor: ResolvedPos,
    $head: ResolvedPos,
    bias⁠?: number
  ) → Selection
  `

  返回跨越给定位置的文本选择，或者，如果它们不是文本位置，则查找它们附近的文本选择。 bias确定该方法是先向前搜索（默认）还是向后搜索（负数）。Selection.near当文档不包含有效的文本位置时，将回退到调用 。

#### class NodeSelection extends Selection

文本选择代表经典的编辑器选择，具有头部(移动的一侧)和锚点(不移动的一侧)，两者都指向`textblock`节点。它可以为空(常规光标位置)。

- `new NodeSelection($pos: ResolvedPos)`

  创建节点选择。不验证其论点的有效性。

- `node: Node`

  选定的节点。

- `static create(doc: Node, from: number) → NodeSelection`

  从未解析的位置创建节点选择。

- `static isSelectable(node: Node) → boolean`

  确定是否可以选择给定节点作为节点选择。

#### class AllSelection extends Selection

表示选择整个文档的选择类型（当文档的开头或结尾存在叶块节点时，不一定可以用文本选择来表示）。

- `new AllSelection(doc: Node)`

  对给定文档创建全选。

#### class SelectionRange

表示文档中选定的范围。

- `new SelectionRange($from: ResolvedPos, $to: ResolvedPos)`

  创建一个范围。

- `$from: ResolvedPos`

  范围的下限。

- `$to: ResolvedPos`

  范围的上限。

#### interface SelectionBookmark

轻量级、独立于文档的选择表示。您可以为自定义选择类定义自定义书签类型，以使历史记录能够很好地处理它。

- `map(mapping: Mappable) → SelectionBookmark`

  通过一组更改映射书签。

- `resolve(doc: Node) → Selection`

  再次将书签解析为真实选择。TextSelection.between这可能需要进行一些错误检查，并且如果映射使书签无效，则可能会回退到默认值（通常 ）。

### 插件系统
为了方便打包和启用额外的编辑器功能，ProseMirror 有一个插件系统。

**`interface PluginSpec<PluginState>`**

这是传递给构造函数的类型Plugin 。它提供了插件的定义。

- `props⁠?: EditorProps<Plugin<PluginState>>`

  该插件添加的视图道具。作为函数的 props 将绑定到插件实例作为其this绑定。

- `state⁠?: StateField<PluginState>`

  允许插件定义一个状态字段，状态对象中的一个额外槽，它可以在其中保存自己的数据。

- `key⁠?: PluginKey`

  可用于使其成为键控插件。在给定状态下，您只能拥有一个具有给定密钥的插件，但可以通过密钥访问插件的配置和状态，而无需访问插件实例对象。

- `view⁠?: fn(view: EditorView) → PluginView`

  当插件需要与编辑器视图交互或在 DOM 中设置某些内容时，请使用此字段。当插件的状态与编辑器视图关联时，将调用该函数。

- `filterTransaction⁠?: fn(tr: Transaction, state: EditorState) → boolean`

  如果存在，它将在状态应用事务之前调用它，允许插件取消它（通过返回 false）。

-
  `
  appendTransaction⁠?: fn(
    transactions: readonly Transaction[],
    oldState: EditorState,
    newState: EditorState
  ) → Transaction
  `

  允许插件在给定的交易数组之后附加要应用的另一个交易。当另一个插件在调用此交易后附加交易时，它会使用新状态和新交易再次调用 - 但只有新交易，即它不会传递它已经看到的交易。

- `[string]: any`

插件规范允许附加属性，可以通过Plugin.spec.

`interface StateField<T>`
插件规范可能会提供这种类型的状态字段（在其 state属性下），它描述了它想要保留的状态。此处提供的函数始终使用插件实例作为其this绑定来调用。

-
  `
  init(
    config: EditorStateConfig,
    instance: EditorState
  ) → T
  `

  初始化该字段的值。config将是传递给 的对象EditorState.create。请注意，这instance是一个半初始化状态实例，并且在此之后初始化的插件字段不会有值。

-
  `
  apply(
    tr: Transaction,
    value: T,
    oldState: EditorState,
    newState: EditorState
  ) → T
  `

  将给定的交易应用于此状态字段，产生一个新的字段值。请注意，该newState参数再次是部分构造的状态，尚未包含此之后插件的状态。

- `toJSON⁠?: fn(value: T) → any`

  将此字段转换为 JSON。可选，可以保留以禁用字段的 JSON 序列化。

-
  `
  fromJSON⁠?: fn(
    config: EditorStateConfig,
    value: any,
    state: EditorState
  ) → T
  `

  反序列化该字段的 JSON 表示形式。请注意，该 state参数再次处于半初始化状态。

#### type PluginView
可以通过插件安装在编辑器中的有状态对象 。

- `update⁠?: fn(view: EditorView, prevState: EditorState)`

  每当视图的状态更新时调用。

- `destroy⁠?: fn()`

  当视图被销毁或接收具有不同插件的状态时调用。

**`class Plugin<PluginState = any>`**

插件捆绑了可以添加到编辑器的功能。它们是编辑器状态的一部分，可能会影响该状态以及包含该状态的视图。

- `new Plugin(spec: PluginSpec<PluginState>)`

  创建一个插件。

- `spec: PluginSpec<PluginState>`

  插件的规范对象。

- `props: EditorProps<Plugin<PluginState>>`

  该插件导出的道具。

- `getState(state: EditorState) → PluginState`

  从编辑器状态中提取插件的状态字段。

**`class PluginKey<PluginState = any>`**

键用于标记插件，以便在给定编辑器状态的情况下找到它们。分配一个键确实意味着在一种状态下只能有一个该类型的插件处于活动状态。

- `new PluginKey(name⁠?: string = "key")`

  创建插件密钥。

- `get(state: EditorState) → Plugin<PluginState>`

  使用此键从编辑器状态获取活动插件（如果有）。

- `getState(state: EditorState) → PluginState`

  从编辑器状态获取插件的状态。

## `@tiptap/pm/tables`

该模块的主文件导出您使用它所需的所有内容。您可能想要做的第一件事是创建一个支持表的架构。这就是tableNodes目的：

- `tableNodes(options: Object) → Object`
此函数为本 模块使用的、和节点类型 创建一组节点规范。然后可以在创建模式时将结果添加到节点集中。`tabletable_row` `table_cell`

  - `options: Object`
  可以理解以下选项：
    - `tableGroup: ?string`
    "block"要添加到表节点类型的 组名称（类似于）。
    - `cellContent: string`
    表格单元格的内容表达式。
    - `cellAttributes: ?Object`
    添加到单元格的附加属性。将属性名称映射到具有以下属性的对象：
      -`default: any`
      该属性的默认值。
      - `getFromDOM: ?fn(dom.Node) → any`
      从 DOM 节点读取属性值的函数。
      - `setDOMAttr: ?fn(value: any, attrs: Object)`
      将属性值添加到用于呈现单元格 DOM 的属性对象的函数。
- `tableEditing() → Plugin`

  创建一个插件 ，当添加到编辑器时，启用单元格选择，处理基于单元格的复制/粘贴，并确保表格保持格式良好（每行具有相同的宽度，并且单元格不重叠）。

  您可能应该将此插件放在插件数组的末尾附近，因为它相当广泛地处理表格中的鼠标和箭头键事件，而其他插件（例如间隙光标或列宽拖动插件）可能需要轮流使用首先执行更具体的行为。

### CellSelection 类扩展了 Selection
Selection 表示跨越表格一部分的单元格选择的子类。启用插件后，当用户跨单元格进行选择时，这些将被创建，并将通过为所选单元格提供 selectedCellCSS 类来绘制。

- `new CellSelection($anchorCell: ResolvedPos, $headCell: ?ResolvedPos = $anchorCell)`

  表选择由其锚点和头部单元格来标识。赋予此构造函数的位置应指向同一个表中的两个单元格之前。它们可能是相同的，以选择单个单元格。

- `$anchorCell: ResolvedPos`

  指向锚单元前面的 已解析位置（扩展选择时不会移动的位置）。

- `$headCell: ResolvedPos`

  指向头部单元前面的已解析位置（扩展选择时该位置会移动）。

- `content() → Slice`

  返回包含选定单元格的表行的矩形切片。

- `isColSelection() → bool`

  如果此选择从表格的顶部一直到底部，则为 True。

- `isRowSelection() → bool`

  如果此选择从表格的左侧一直到右侧，则为 True。

- `static colSelection($anchorCell: ResolvedPos, $headCell: ?ResolvedPos = $anchorCell) → CellSelection`

  返回覆盖给定锚点和标题单元格的最小列选择。

- `static rowSelection($anchorCell: ResolvedPos, $headCell: ?ResolvedPos = $anchorCell) → CellSelection`

  返回覆盖给定锚点和头单元格的最小行选择。

- `static create(doc: Node, anchorCell: number, headCell: ?number = anchorCell) → CellSelection`

### 命令

以下命令可用于向用户提供表编辑功能。

- `addColumnBefore(state: EditorState, dispatch: ?fn(tr: Transaction)) → bool`

  在包含选择的列之前添加一列的命令。

- `addColumnAfter(state: EditorState, dispatch: ?fn(tr: Transaction)) → bool`

  用于在包含选择的列之后添加一列的命令。

- `deleteColumn(state: EditorState, dispatch: ?fn(tr: Transaction)) → bool`

  从表中删除选定列的命令功能。

- `addRowBefore(state: EditorState, dispatch: ?fn(tr: Transaction)) → bool`

  在选择之前添加一个表格行。

- `addRowAfter(state: EditorState, dispatch: ?fn(tr: Transaction)) → bool`

  选择后添加一个表格行。

- `deleteRow(state: EditorState, dispatch: ?fn(tr: Transaction)) → bool`

  从表中删除选定的行。

- `mergeCells(state: EditorState, dispatch: ?fn(tr: Transaction)) → bool`

  将选定的单元格合并为一个单元格。仅当选定单元格的轮廓形成矩形时才可用。

- `splitCell(state: EditorState, dispatch: ?fn(tr: Transaction)) → bool`

  将 `rowpan` 或 `colspan` 大于 1 的选定单元格拆分为更小的单元格。使用第一种细胞类型作为新细胞。

- `splitCellWithType(getType: fn({row: number, col: number, node: Node}) → NodeType) → fn(EditorState, dispatch: ?fn(tr: Transaction)) → bool`
  将 `rowpan` 或 `colspan` 大于 1 的选定单元格拆分为更小的单元格，其单元格类型为 getType 函数返回的单元格类型 (th, td)。

- `setCellAttr(name: string, value: any) → fn(EditorState, dispatch: ?fn(tr: Transaction)) → bool`

  返回一个将给定属性设置为给定值的命令，并且仅当当前选定的单元格尚未将该属性设置为该值时才可用。

- `toggleHeaderRow(EditorState, dispatch: ?fn(tr: Transaction)) → bool`

  切换所选行是否包含标题单元格。

- `toggleHeaderColumn(EditorState, dispatch: ?fn(tr: Transaction)) → bool`

  切换所选列是否包含标题单元格。

- `toggleHeaderCell(EditorState, dispatch: ?fn(tr: Transaction)) → bool`

  切换所选单元格是否为标题单元格。

- `toggleHeader(type: string, options: ?{useDeprecatedLogic: bool}) → fn(EditorState, dispatch: ?fn(tr: Transaction)) → bool`

  在行/列标题和普通单元格之间切换（仅适用于第一行/列）。对于已弃用的行为，请传入`useDeprecatedLogic:true` 选项。

- `goToNextCell(direction: number) → fn(EditorState, dispatch: ?fn(tr: Transaction)) → bool`

  返回用于选择表中下一个（方向=1）或上一个（方向=-1）单元格的命令。

- `deleteTable(state: EditorState, dispatch: ?fn(tr: Transaction)) → bool`

  删除所选内容周围的表格（如果有）。

### 公用事业
-`fixTables(state: EditorState, oldState: ?EditorState) → ?Transaction`

  检查给定状态文档中的所有表，并在必要时返回修复它们的事务。如果oldState提供，则假定保持先前的已知良好状态，这将用于避免重新扫描文档中未更改的部分。

### 类表映射
表映射描述了给定表的结构。为了避免一直重新计算它们，它们被每个表节点缓存。为了能够做到这一点，地图中保存的位置相对于表格的开头，而不是文档的开头。

- `width: number`

  table的宽度

- `height: number`

  table的高度

- `map: [number]`

  宽度 * 高度数组，其中单元格的起始位置覆盖每个槽中表格的该部分

- `findCell(pos: number) → Rect`

  求给定位置的单元格的尺寸。

- `colCount(pos: number) → number`

  在给定位置找到单元格的左侧。

- `nextCell(pos: number, axis: string, dir: number) → ?number`

  从 处的单元格（pos如果有）开始，查找给定方向上的下一个单元格。

- `rectBetween(a: number, b: number) → Rect`

  获取跨越两个给定单元格的矩形。

- `cellsInRect(rect: Rect) → [number]`

  返回给定矩形中左上角的所有单元格的位置。

- `positionAt(row: number, col: number, table: Node) → number`

  返回给定行和列的单元格开始的位置，或者如果单元格从那里开始，则返回该位置。

- `static get(table: Node) → TableMap`

  查找给定表节点的表映射。

## `@tiptap/pm/transform`

该模块定义了一种修改文档的方法，允许记录、重播和重新排序更改。您可以在指南中阅读有关转换的更多信息。

### 转换步骤
转换发生在Steps 中，这是对文档的原子的、定义明确的修改。应用一个步骤会生成一个新文档。

每个步骤都提供一个更改映射，将旧文档中的位置映射到转换后的文档中的位置。可以反转步骤以创建一个撤消其效果的步骤，并将其链接在一个称为 的便利对象中Transform。

#### abstract class Step
步骤对象代表原子更改。它通常仅适用于为其创建的文档，因为其中存储的位置仅对该文档有意义。

新步骤是通过创建扩展Step、重写apply、invert、map、getMap和fromJSON方法，并使用`.Step.jsonID`.

- `abstract apply(doc: Node) → StepResult`

  将此步骤应用于给定文档，返回一个结果对象，如果该步骤无法应用于此文档，则该结果对象指示失败，或者通过包含转换后的文档来指示成功。

- `getMap() → StepMap`

  获取表示此步骤所做更改的步骤图，可用于在旧文档和新文档中的位置之间进行转换。

- `abstract invert(doc: Node) → Step`

  创建此步骤的反转版本。需要该步骤之前的文档作为参数。

- `abstract map(mapping: Mappable) → Step`

  通过可映射事物映射此步骤，返回该步骤的位置已调整的版本，或者null该步骤是否已被映射完全删除。

- `merge(other: Step) → Step`

  尝试将此步骤与另一步骤合并，然后直接应用。如果可能，则返回合并的步骤；如果步骤无法合并，则返回 null。

- `abstract toJSON() → any`

  创建此步骤的 JSON 可序列化表示。为自定义子类定义此属性时，请确保结果对象在属性下包含步骤类型的JSON idstepType。

- `static fromJSON(schema: Schema, json: any) → Step`

  从 JSON 表示中反序列化一个步骤。将调用步骤类自己的此方法的实现。

-
  `
  static jsonID(
    id: string,
    stepClass: {fromJSON: fn(schema: Schema, json: any) → Step}
  ) → {fromJSON: fn(schema: Schema, json: any) → Step}
  `

  为了能够将步骤序列化为 JSON，每个步骤都需要一个字符串 ID 附加到其 JSON 表示形式。使用此方法为您的步骤类注册 ID。尝试选择不太可能与其他模块的步骤发生冲突的内容。

#### class StepResult
应用步骤的结果。包含新文档或失败值。

- `doc: Node`

  如果成功，则转换后的文档。

- `failed: string`

  如果不成功，则显示失败消息。

- `static ok(doc: Node) → StepResult`

  创建成功的步骤结果。

- `static fail(message: string) → StepResult`

  创建失败的步骤结果。

-
  `
  static fromReplace(
    doc: Node,
    from: number,
    to: number,
    slice: Slice
  ) → StepResult
  `

  Node.replace使用给定的参数进行调用。如果成功则创建一个成功的结果，如果抛出ReplaceError.

#### class ReplaceStep extends Step
用一段新内容替换文档的一部分。

-
  `
  new ReplaceStep(
    from: number,
    to: number,
    slice: Slice,
    structure⁠?: boolean = false
  )
  `
  给定的应该适合和 slice之间的“间隙” ——深度必须对齐，并且周围的节点必须能够与切片的开放侧连接。当 为 true 时，如果 from 和 to 之间的内容不仅仅是关闭然后打开标记的序列（这是为了防止重新基址替换步骤覆盖它们不应该覆盖的内容），则该步骤将失败。fromtostructure

- `from: number`

  替换范围的起始位置。

- `to: number`

  替换范围的结束位置。

- `slice: Slice`

  要插入的切片。

#### class ReplaceAroundStep extends Step
将文档的一部分替换为内容切片，但通过将替换内容移动到切片中来保留替换内容的范围。

-
  `
  new ReplaceAroundStep(
    from: number,
    to: number,
    gapFrom: number,
    gapTo: number,
    slice: Slice,
    insert: number,
    structure⁠?: boolean = false
  )
  `

  使用给定的范围和间隙创建替换步骤。 insert应该是切片中间隙内容应移动到的点。structure与课堂上的含义相同ReplaceStep。

- `from: number`

  替换范围的起始位置。

- `to: number`

  替换范围的结束位置。

- `gapFrom: number`

  保留范围的开始。

- `gapTo: number`

  保留范围的末端。

- `slice: Slice`

  要插入的切片。

- `insert: number`

  切片中应插入保留范围的位置。

#### class AddMarkStep extends Step
为两个位置之间的所有内联内容添加标记。

- `new AddMarkStep(from: number, to: number, mark: Mark)`

  创建标记步骤。

- `from: number`

  标记范围的开始。

- `to: number`

  标记范围的末尾。

- `mark: Mark`

  要添加的标记。

#### class RemoveMarkStep extends Step
从两个位置之间的所有内联内容中删除标记。

- `new RemoveMarkStep(from: number, to: number, mark: Mark)`

  创建标记删除步骤。

- `from: number`

  未标记范围的开始。

- `to: number`

  未标记范围的末尾。

- `mark: Mark`

  要删除的标记。

#### class AddNodeMarkStep extends Step
为特定节点添加标记。

- `new AddNodeMarkStep(pos: number, mark: Mark)`

  创建节点标记步骤。

- `pos: number`

  目标节点的位置。

- `mark: Mark`

  要添加的标记。

#### class RemoveNodeMarkStep extends Step
从特定节点删除标记。

- `new RemoveNodeMarkStep(pos: number, mark: Mark)`

  创建标记删除步骤。

- `pos: number`

  目标节点的位置。

- `mark: Mark`

  要删除的标记。

#### class AttrStep extends Step
更新特定节点中的属性。

- `new AttrStep(pos: number, attr: string, value: any)`

  构建属性步骤。

- `pos: number`

  目标节点的位置。

- `attr: string`

  要设置的属性。

- `value: any`

- `static fromJSON(schema: Schema, json: any) → AttrStep`

### 位置映射
通过运行 步骤生成的步骤图将位置从一个文档映射到另一个文档是 ProseMirror 中的一项重要操作。例如，它用于在文档更改时更新选择。

#### interface Mappable
可以通过多种方式来映射位置。此类对象符合此接口。

- `map(pos: number, assoc⁠?: number) → number`

  通过该对象映射位置。给定时，assoc（应为 -1 或 1，默认为 1）确定该位置与哪一侧相关联，这决定了在映射位置插入内容块时要向哪个方向移动。

- `mapResult(pos: number, assoc⁠?: number) → MapResult`

  映射一个位置，并返回一个包含有关映射的附加信息的对象。结果的deleted字段告诉您在映射过程中该位置是否被删除（完全包含在替换范围内）。assoc当仅删除一侧的内容时，只有当该位置指向被删除内容的方向时，才认为该位置本身被删除 。

#### class MapResult
表示带有额外信息的映射位置的对象。

- `pos: number`

  位置的映射版本。

- `deleted: boolean`

  告诉您该位置是否已删除，即该步骤是否assoc从文档中删除了（通过 ）参数查询一侧的标记。

- `deletedBefore: boolean`

  告诉您映射位置之前的标记是否已删除。

- `deletedAfter: boolean`

  当映射位置之后的标记被删除时为 True。

- `deletedAcross: boolean`

  告知是否有任何通过删除映射的步骤跨过该位置（包括该位置之前和之后的标记）。

#### class StepMap implements Mappable
描述步骤所做的删除和插入的映射，可用于查找文档的前步骤版本中的位置与后步骤版本中的相同位置之间的对应关系。

-
  `
  new StepMap(
    ranges: readonly number[],
    inverted⁠?: boolean = false
  )
  `

  创建位置图。对文档的修改表示为一个数字数组，其中每三个一组表示一个修改的块，如[start, oldSize, newSize]。

-
  `
  forEach(
    f: fn(
      oldStart: number,
      oldEnd: number,
      newStart: number,
      newEnd: number
    )
  )
  `

  在此映射中包含的每个更改范围上调用给定函数。

- `invert() → StepMap`

  创建此地图的倒置版本。结果可用于将后步骤文档中的位置映射到前步骤文档。

- `static offset(n: number) → StepMap`

创建一个按偏移量（可能为负数）移动所有位置的地图。当将子文档的步骤应用于较大的文档时，这非常有用，反之亦然。

- `static empty: StepMap`

  不包含任何更改范围的 StepMap。

#### class Mapping implements Mappable
映射表示零个或多个步骤映射的管道。它具有通过一系列步骤无损处理映射位置的特殊规定，其中某些步骤是早期步骤的反转版本。（当为协作或历史管理“重新设定基准”步骤时，会出现这种情况。）

-
  `
  new Mapping(
    maps⁠?: StepMap[] = [],
    mirror⁠?: number[],
    from⁠?: number = 0,
    to⁠?: number = maps.length
  )
  `

  使用给定的位置图创建新的映射。

- `maps: StepMap[]`

  该映射中的步骤映射。

- `from: number`

  数组中的起始位置，在调用or maps时使用。mapmapResult

- `to: number`

  数组中的结束位置maps。

-
  `
  slice(
    from⁠?: number = 0,
    to⁠?: number = this.maps.length
  ) → Mapping
  `

  创建一个仅映射该映射的一部分的映射。

- `appendMap(map: StepMap, mirrors⁠?: number)`

  将步骤图添加到该映射的末尾。如果mirrors给定，它应该是步骤图的索引，该索引是该步骤图的镜像。

- `appendMapping(mapping: Mapping)`

  将给定映射中的所有步骤映射添加到该映射中（保留镜像信息）。

- `getMirror(n: number) → number`

  在此映射中查找在给定偏移处镜像映射的步骤映射的偏移量（根据 的第二个参数 appendMap）。

- `appendMappingInverted(mapping: Mapping)`

  将给定映射的逆映射附加到该映射。

- `invert() → Mapping`

  创建此映射的反转版本。

- `map(pos: number, assoc⁠?: number = 1) → number`

  通过这个映射来映射一个位置。

- `mapResult(pos: number, assoc⁠?: number = 1) → MapResult`

  通过这个映射映射一个位置，返回一个映射结果。

### 文档转换
因为您经常需要将多个步骤收集在一起以实现复合更改，所以 ProseMirror 提供了一个抽象来使这变得容易。状态事务是转换的子类。

#### class Transform
用于构建和跟踪 表示文档转换的一系列步骤的抽象。

大多数转换方法返回Transform对象本身，以便它们可以链接起来。

- `new Transform(doc: Node)`

  创建以给定文档开头的转换。

- `steps: Step[]`

  此变换中的步骤。

- `docs: Node[]`

  每个步骤之前的文档。

- `mapping: Mapping`

  包含此转换中每个步骤的映射的映射。

- `doc: Node`

  当前文档（应用转换中的步骤的结果）。

- `before: Node`

  起始文档。

- `step(step: Step) → Transform`

  在此转换中应用新步骤，保存结果。当步骤失败时抛出错误。

- `maybeStep(step: Step) → StepResult`

  尝试在此转换中应用一个步骤，如果失败则忽略它。返回步骤结果。

- `docChanged: boolean`

  当文档已更改时为真（当有任何步骤时）。

-
  `
  replace(
    from: number,
    to⁠?: number = from,
    slice⁠?: Slice = Slice.empty
  ) → Transform
  `

  from将文档中和之间的部分替换to为给定的slice。

-
  `
  replaceWith(
    from: number,
    to: number,
    content: Fragment | Node | readonly Node[]
  ) → Transform
  `
  将给定范围替换为给定内容，给定内容可以是片段、节点或节点数组。

- `delete(from: number, to: number) → Transform`

  删除给定位置之间的内容。

-
  `
  insert(
    pos: number,
    content: Fragment | Node | readonly Node[]
  ) → Transform
  `

  在给定位置插入给定内容。

- `replaceRange(from: number, to: number, slice: Slice) → Transform`

  使用给定切片替换文档的范围，使用 from、to和切片的 openStart属性作为提示，而不是固定的起点和终点。此方法可以增长替换区域或关闭切片中的开放节点，以便获得更符合所见即所得期望的拟合，方法是在替换区域被标记为非定义上下文时删除完全覆盖的父节点，或者包括切片中标记为定义其内容的开放父节点。

  例如，这就是处理粘贴的方法。类似的 replace方法是一种更原始​​的工具，它不会移动其给定范围的起点和终点，并且在您需要更精确地控制所发生的情况的情况下很有用。

- `replaceRangeWith(from: number, to: number, node: Node) → Transform`

  用节点替换给定范围，但使用from和to作为提示，而不是精确位置。当 from 和 to 相同并且位于不适合给定节点的父节点的开头或结尾时，此方法可能会将它们移向允许放置给定节点的父节点。当给定的范围完全覆盖一个父节点时，该方法可能会完全替换该父节点。

- `deleteRange(from: number, to: number) → Transform`

  删除给定范围，将其扩展以覆盖完全覆盖的父节点，直到找到有效的替换。

- `lift(range: NodeRange, target: number) → Transform`

  如果给定范围内的内容之前或之后存在同级内容，则将给定范围内的内容与其父级内容分开，并将其在树中向上移动到 指定的深度target。您可能需要使用 liftTarget来 计算target，以确保电梯有效。

- `join(pos: number, depth⁠?: number = 1) → Transform`

  将给定位置周围的块连接起来。如果深度为 2，则它们的最后一个和第一个兄弟姐妹也会加入，依此类推。

-
  `
  wrap(
    range: NodeRange,
    wrappers: readonly {type: NodeType, attrs⁠?: Attrs}[]
  ) → Transform
  `

  将给定范围包装在给定的包装器集中。假定包装器在此位置有效，并且可能应该使用 进行计算findWrapping。

-
  `
  setBlockType(
    from: number,
    to⁠?: number = from,
    type: NodeType,
    attrs⁠?: Attrs = null
  ) → Transform
  `

  将所有文本块的类型（部分）设置from为to具有给定属性的给定节点类型。

-
  `
  setNodeMarkup(
    pos: number,
    type⁠?: NodeType,
    attrs⁠?: Attrs = null,
    marks⁠?: readonly Mark[]
  ) → Transform
  `

  更改pos节点的类型、属性和/或标记。如果未指定type，则保留现有节点的类型;

- `setNodeAttribute(pos: number, attr: string, value: any) → Transform`

  将给定节点上的单个属性设置为新值。

- `addNodeMark(pos: number, mark: Mark) → Transform`

  向位置 处的节点添加标记pos。

- `removeNodeMark(pos: number, mark: Mark | MarkType) → Transform`

  从位置 处的节点中删除一个标记（或给定类型的标记）pos。

-
  `
  split(
    pos: number,
    depth⁠?: number = 1,
    typesAfter⁠?: {type: NodeType, attrs⁠?: Attrs}[]
  ) → Transform
  `

  在给定位置分割节点，并且可选地，如果depth大于 1，则分割该位置之上的任意数量的节点。默认情况下，分割出来的部分将继承原始节点的节点类型。这可以通过传递要在拆分后使用的类型和属性数组来更改。

- `addMark(from: number, to: number, mark: Mark) → Transform`

  将给定标记添加到from和之间的内联内容中to。

-
  `
  removeMark(
    from: number,
    to: number,
    mark⁠?: Mark | MarkType | null
  ) → Transform
  `

  from删除和之间的内联节点的标记to。当 mark是单个标记时，精确地删除该标记。如果是标记类型，则删除该类型的所有标记。当为空时，删除所有类型的标记。

-
  `
  clearIncompatible(
    pos: number,
    parentType: NodeType,
    match⁠?: ContentMatch
  ) → Transform
  `

  从节点内容中删除与 pos给定的新父节点类型不匹配的所有标记和节点。接受可选的起始内容匹配作为第三个参数。

  在创建转换或确定转换是否可行时，以下辅助函数非常有用。

-
  `
  replaceStep(
    doc: Node,
    from: number,
    to⁠?: number = from,
    slice⁠?: Slice = Slice.empty
  ) → Step
  `

  将切片“适合”文档中的给定位置，生成 插入它的步骤。如果没有有意义的方式在此处插入切片，或者插入它将是无操作（空范围内的空切片），则将返回 null。

- `liftTarget(range: NodeRange) → number`

  尝试找到给定范围内的内容可以提升到的目标深度。不会跨越 隔离父节点。

-
  `
  findWrapping(
    range: NodeRange,
    nodeType: NodeType,
    attrs⁠?: Attrs = null,
    innerRange⁠?: NodeRange = range
  ) → {type: NodeType, attrs: Attrs}[]
  `

  尝试找到一种有效的方法将给定范围内的内容包装在给定类型的节点中。如有必要，可以在包装器节点周围和内部引入额外的节点。如果找不到有效的包装，则返回 null。当innerRange给出 时，该范围的内容将用作适合包装的内容，而不是 的内容range。

-
  `
  canSplit(
    doc: Node,
    pos: number,
    depth⁠?: number = 1,
    typesAfter⁠?: {type: NodeType, attrs⁠?: Attrs}[]
  ) → boolean
  `

  检查给定位置是否允许分裂。

- `canJoin(doc: Node, pos: number) → boolean`

  测试给定位置前后的块是否可以连接。

- `joinPoint(doc: Node, pos: number, dir⁠?: number = -1) → number`

  dir找到给定位置的祖先，该祖先可以在之前（或之后，如果为正）连接到块。返回可连接点（如果有）。

- `insertPoint(doc: Node, pos: number, nodeType: NodeType) → number`

  当给定类型的节点不是有效位置但位于节点的开头或结尾pos时，通过搜索节点层次结构，尝试找到可以在 附近插入给定类型的节点的点。pos如果未找到位置，则返回 null。

- `dropPoint(doc: Node, pos: number, slice: Slice) → number`

  查找给定位置或其周围可以插入给定切片的位置。将查看父节点最近的边界并在那里尝试，即使原始位置不直接位于该节点的开头或结尾。未找到位置时返回 null。

## `@tiptap/pm/view`

ProseMirror 的视图模块在 DOM 中显示给定的编辑器状态，并处理用户事件。

使用此模块时请确保加载`style/prosemirror.css`为样式表。

#### class EditorView
编辑器视图管理表示可编辑文档的 DOM 结构。它的状态和行为是由它的 props决定的。

- `new EditorView( place: DOMNode | fn(editor: HTMLElement) | {mount: HTMLElement} | null, props: DirectEditorProps )`

  创建一个视图。place可能是编辑器应附加到的 DOM 节点、将其放入文档的函数，或者其mount属性保存要用作文档容器的节点的对象。如果是null，编辑器将不会添加到文档中。

- `state: EditorState`

  视图的当前状态。

- `dom: HTMLElement`

  包含文档的可编辑 DOM 节点。（您可能不应该直接干扰其内容。）

- `editable: boolean`

  指示编辑器当前是否可编辑。

- `dragging: {slice: Slice, move: boolean}`

  当编辑器内容被拖动时，该对象包含有关拖动切片以及是否正在复制或移动的信息。在任何其他时间，它都是空的。

- `composing: boolean`

  当组合处于活动状态时，该条件成立。

- `props: DirectEditorProps`

  视图当前的props。

- `update(props: DirectEditorProps)`

  更新视图的道具。将立即导致 DOM 更新。

- `setProps(props: Partial<DirectEditorProps>)`

  通过使用作为参数给出的对象更新现有的 props 对象来更新视图。相当于`view.update(Object.assign({}, view.props, props))`.

- `updateState(state: EditorState)`

  更新编辑器的state道具，而不触及任何其他道具。

-
  `
  someProp<PropName extends keyof EditorProps, Result>(
    propName: PropName,
    f: fn(
      value: NonNullable<EditorProps[PropName]>
    ) → Result
  ) → Result
  `

  `someProp<PropName extends keyof EditorProps>(propName: PropName) → NonNullable<EditorProps[PropName]>`

  遍历 prop 的值，首先是直接提供的值，然后是从插件提供给视图的值，然后是从状态中的插件（按顺序），并在每次f找到非未定义值时调用。当f返回真值时，立即返回。当f未提供时，它被视为恒等函数（直接返回 prop 值）。

- `hasFocus() → boolean`

  查询视图是否具有焦点。

- `focus()`

  集中编辑器。

- `root: Document | ShadowRoot`

  获取编辑器所在的文档根目录。这通常是顶层document，但 如果编辑器位于其中，则可能是影子 DOM根。

- `posAtCoords(coords: {left: number, top: number}) → {pos: number, inside: number}`

  给定一对视口坐标，返回与它们对应的文档位置。如果给定的坐标不在编辑器内，则可能返回 null。当返回一个对象时，它的pos属性是最接近坐标的位置，并且它的inside属性保存该位置所在的内部节点的位置，如果它位于顶层而不是在任何节点中，则为-1。

- `coordsAtPos(pos: number, side⁠?: number = 1) → {left: number, right: number, top: number, bottom: number}`

  返回给定文档位置的视口矩形。 left和right将是相同的数字，因为这会返回一个平坦的光标矩形。如果位置位于不直接相邻的两个事物之间，side则确定使用哪个元素。当 < 0 时，使用该位置之前的元素，否则使用之后的元素。

- `domAtPos(pos: number, side⁠?: number = 0) → {node: DOMNode, offset: number}`

  查找与给定文档位置对应的 DOM 位置。当side为负数时，寻找尽可能接近该位置之前内容的位置。当为正值时，优先选择靠近内容的位置。当为零时，首选尽可能浅的位置。

  请注意，您不应该改变编辑器的内部 DOM，而只检查它（甚至通常没有必要）。

- `nodeDOM(pos: number) → DOMNode`

  查找表示给定位置之后的文档节点的 DOM 节点。null当位置未指向节点前面或节点位于不透明节点视图内时可能会返回。

  这是为了能够调用 getBoundingClientRectDOM 节点上的东西。不要直接改变编辑器 DOM，或以这种方式添加样式，因为编辑器在重绘节点时会立即覆盖它。

-
  `
  posAtDOM(
    node: DOMNode,
    offset: number,
    bias⁠?: number = -1
  ) → number
  `

  查找与给定 DOM 位置对应的文档位置。（只要有可能，最好直接检查文档结构，而不是在 DOM 中摸索，但有时（例如在解释事件目标时）您别无选择。）

  当位置位于叶节点内部时，该bias参数可用于影响使用 DOM 节点的哪一侧。

-
  `
  endOfTextblock(
    dir: "up" |
    "down" |
    "left" |
    "right" |
    "forward" |
    "backward"
    ,
    state⁠?: EditorState
  ) → boolean
  `

  确定沿给定方向移动时所选内容是否位于文本块的末尾。例如，当给定 时"left"，如果从当前光标位置向左移动将离开该位置的父文本块，则它将返回 true。默认情况下将应用于视图的当前状态，但可以传递不同的状态。

- `pasteHTML(html: string, event⁠?: ClipboardEvent) → boolean`

  使用给定的 HTML 字符串运行编辑器的粘贴逻辑。event如果给出，将被传递给 钩子 handlePaste。

- `pasteText(text: string, event⁠?: ClipboardEvent) → boolean`

  使用给定的纯文本输入运行编辑器的粘贴逻辑。

- `destroy()`

  从 DOM 中删除编辑器并销毁所有节点视图。

- `isDestroyed: boolean`

  当视图已被 销毁（因此不应再使用）时，情况就是如此。

- `dispatchEvent(event: Event)`

  用于测试。

- `dispatch(tr: Transaction)`

  发送交易。将在给定时调用 dispatchTransaction ，否则默认将事务应用到当前状态并使用 updateState结果进行调用。该方法绑定到视图实例，以便可以轻松传递。

### Props

`interface EditorProps<P = any>`

  Props 是可以传递到编辑器视图或包含在插件中的配置值。该界面列出了支持的道具。

  各种事件处理函数都可以返回true以指示它们处理了给定事件。然后，视图将小心地调用preventDefault事件，但使用 时除外 handleDOMEvents，其中处理程序本身负责该事件。

  如何解决 prop 取决于 prop。处理函数一次调用一个，从基本 props 开始，然后搜索插件（按出现顺序），直到其中一个返回 true。对于某些 props，第一个产生值的插件优先。

  可选的 type 参数指的是 in prop 函数的类型this，用于在定义 插件时传入插件类型。

-
  `
  handleDOMEvents⁠?: {
    [event in keyof DOMEventMap]: fn(
    view: EditorView,
    event: DOMEventMap[event]
    ) → boolean | undefined
  }
  `

  可以是将 DOM 事件类型名称映射到处理它们的函数的对象。此类函数将在对可编辑 DOM 元素上触发的事件进行任何处理 ProseMirror 之前调用。与其他事件处理属性相反，当从这样的函数返回 true 时，您有责任调用 preventDefault自己（或者不调用自己，如果您想允许默认行为）。

- `handleKeyDown⁠?: fn(view: EditorView, event: KeyboardEvent) → boolean | undefined`

  当编辑器收到keydown事件时调用。

- `handleKeyPress⁠?: fn(view: EditorView, event: KeyboardEvent) → boolean | undefined`

  事件处理程序keypress。

-
  `
  handleTextInput⁠?: fn(
    view: EditorView,
    from: number,
    to: number,
    text: string
  ) → boolean | undefined
  `

  每当用户直接输入文本时，都会在应用输入之前调用此处理程序。如果它返回true，则实际插入文本的默认行为将被抑制。

-
  `
  handleClickOn⁠?: fn(
    view: EditorView,
    pos: number,
    node: Node,
    nodePos: number,
    event: MouseEvent,
    direct: boolean
  ) → boolean | undefined
  `

  围绕每个节点调用一次单击，从内到外。对于内部节点，该 direct标志将为 true。

-
  `
  handleClick⁠?: fn(
    view: EditorView,
    pos: number,
    event: MouseEvent
  ) → boolean | undefined
  `

  在调用处理程序后单击编辑器时调用handleClickOn。

-
  `
  handleDoubleClickOn⁠?: fn(
    view: EditorView,
    pos: number,
    node: Node,
    nodePos: number,
    event: MouseEvent,
    direct: boolean
  ) → boolean | undefined
  `

  围绕双击调用每个节点。

-
  `
  handleDoubleClick⁠?: fn(
    view: EditorView,
    pos: number,
    event: MouseEvent
  ) → boolean | undefined
  `

  双击编辑器时调用，之后handleDoubleClickOn。

-
  `
  handleTripleClickOn⁠?: fn(
    view: EditorView,
    pos: number,
    node: Node,
    nodePos: number,
    event: MouseEvent,
    direct: boolean
  ) → boolean | undefined
  `

  围绕三次点击调用每个节点。

-
  `
  handleTripleClick⁠?: fn(
    view: EditorView,
    pos: number,
    event: MouseEvent
  ) → boolean | undefined
  `

  在编辑器三次单击后调用handleTripleClickOn。

-
  `
  handlePaste⁠?: fn(
    view: EditorView,
    event: ClipboardEvent,
    slice: Slice
  ) → boolean | undefined
  `

  可用于覆盖粘贴行为。slice是编辑器解析的粘贴内容，但您可以直接访问该事件以获取原始内容。

-
  `
  handleDrop⁠?: fn(
    view: EditorView,
    event: DragEvent,
    slice: Slice,
    moved: boolean
  ) → boolean | undefined
  `

  当有东西落在编辑器上时调用。moved如果此放置从当前选择中移动（因此应将其删除），则为 true。

- `handleScrollToSelection⁠?: fn(view: EditorView) → boolean`

  当视图更新其状态后尝试将所选内容滚动到视图中时调用。处理程序函数可能返回 false 以指示它未处理滚动，并且应尝试进一步的处理程序或默认行为。

-
  `
  createSelectionBetween⁠?: fn(
    view: EditorView,
    anchor: ResolvedPos,
    head: ResolvedPos
  ) → Selection
  `

  可用于覆盖在读取给定锚点和头部之间的 DOM 选择时创建选择的方式。

- `domParser⁠?: DOMParser`

  读取编辑器从 DOM 更改时使用的解析器。默认调用 `DOMParser.fromSchema`编辑器的架构。

- `transformPastedHTML⁠?: fn(html: string, view: EditorView) → string`

  可用于在解析粘贴的 HTML 文本之前对其进行转换，例如清理它。

- `clipboardParser⁠?: DOMParser`

  从剪贴板读取内容时使用的解析器。domParser如果未给出，则使用 prop的值 。

-
  `
  transformPastedText⁠?: fn(
    text: string,
    plain: boolean,
    view: EditorView
  ) → string
  `

  转换粘贴的纯文本。plain当文本粘贴为纯文本时，该标志将为 true。

-
  `
  clipboardTextParser⁠?: fn(
    text: string,
    $context: ResolvedPos,
    plain: boolean,
    view: EditorView
  ) → Slice
  `

  将剪贴板中的文本解析为文档切片的函数。之后打电话 transformPastedText。默认行为是将文本分割成行，将它们包装在<p>标签中，然后调用 clipboardParser它。plain当文本粘贴为纯文本时，该标志将为 true。

- `transformPasted⁠?: fn(slice: Slice, view: EditorView) → Slice`

  可用于在将粘贴或拖放的内容应用于文档之前对其进行转换。

- `transformCopied⁠?: fn(slice: Slice, view: EditorView) → Slice`

  可用于在将复制或剪切的内容序列化到剪贴板之前对其进行转换。

- `nodeViews⁠?: Object<NodeViewConstructor>`

  允许您传递节点的自定义渲染和行为逻辑。NodeView应该将节点名称映射到生成实现节点显示行为的对象的构造函数。第三个参数getPos是一个函数，可以调用该函数来获取节点的当前位置，这在创建事务来更新它时非常有用。请注意，如果该节点不在文档中，则该函数返回的位置将为undefined。

  decorations是节点或节点周围活动的内联装饰的数组。它们以正常方式自动绘制，您通常只想忽略这一点，但它们也可以用作向节点视图提供上下文信息的方式，而无需将其添加到文档本身。

  innerDecorations保存节点内容的装饰。如果您的视图没有内容或属性 ，您可以放心地忽略这一点contentDOM，因为编辑器将在内容上绘制装饰。但是，例如，如果您想要创建一个包含内容的嵌套编辑器，则为其提供内部装饰可能是有意义的。

  （出于向后兼容性的原因，标记视图也可以包含在此对象中。）

- `markViews⁠?: Object<MarkViewConstructor>`

  传递自定义标记渲染函数。请注意，这些不能提供节点视图可以提供的动态行为——它们只是提供自定义渲染逻辑。第三个参数指示标记的内容是否内联。

- `clipboardSerializer⁠?: DOMSerializer`

  将内容放入剪贴板时使用的 DOM 序列化器。DOMSerializer.fromSchema 如果未给出，则将使用的结果 。该对象只会 serializeFragment 调用其方法，并且您可以提供实现兼容方法的替代对象类型。

- `clipboardTextSerializer⁠?: fn(content: Slice, view: EditorView) → string`

  将文本复制到剪贴板时将调用该函数来获取当前选择的文本。默认情况下，编辑器将textBetween在选定的范围内使用。

- `decorations⁠?: fn(state: EditorState) → DecorationSource`

  要在视图中显示的一组文档装饰。

- `editable⁠?: fn(state: EditorState) → boolean`

  当返回 false 时，视图的内容不可直接编辑。

- `attributes⁠?: Object<string> | fn(state: EditorState) → Object<string>`

  控制可编辑元素的 DOM 属性。可以是一个对象，也可以是从编辑器状态到对象的函数。默认情况下，元素将获得一个 class "ProseMirror"，并且其contentEditable属性由 editable prop确定。此处提供的其他课程将添加到该课程中。someProp对于其他属性，将使用首先提供的值（如 中 ）。

- `scrollThreshold⁠?: number | {top: number, right: number, bottom: number, left: number}`

  确定光标与可见视口末端之间的距离（以像素为单位），当将光标滚动到视图中时，会在该点发生滚动。默认为 0。

- `scrollMargin⁠?: number | {top: number, right: number, bottom: number, left: number}`

  确定当光标滚动到视图中时光标上方或下方留下的额外空间（以像素为单位）。默认为 5。

-
  `
  type NodeViewConstructor = fn(
    node: Node,
    view: EditorView,
    getPos: fn() → number,
    decorations: readonly Decoration[],
    innerDecorations: DecorationSource
  ) → NodeView
  `

  提供创建节点视图的函数类型。

-
  `
  type MarkViewConstructor = fn(
    mark: Mark,
    view: EditorView,
    inline: boolean
  ) → {dom: HTMLElement, contentDOM⁠?: HTMLElement}
  `

  用于创建标记视图的函数类型。

**interface DirectEditorProps extends EditorProps**

直接提供给编辑器视图的 props 对象支持一些不能在插件中使用的字段：

- `state: EditorState`

  编辑器的当前状态。

- `plugins⁠?: readonly Plugin[]`

  一组在视图中使用的插件，应用它们的插件 view和 props。使用状态组件（状态字段字段或 事务过滤器或附加程序）传递插件将导致错误，因为此类插件必须存在于状态中才能工作。

- `dispatchTransaction⁠?: fn(tr: Transaction)`

  用于发送视图生成的事务（状态更新）的回调。如果您指定了这一点，您可能希望确保最终 以应用了updateState事务的新状态调用视图的方法 。回调将绑定到视图实例作为其绑定。this

**interface NodeView**

toDOM默认情况下，文档节点使用其规范方法的结果进行渲染 ，并完全由编辑器管理。对于某些用例，例如嵌入的特定于节点的编辑界面，您希望更好地控制节点编辑器内表示的行为，并且需要定义 自定义节点视图。

标记视图仅支持dom和contentDOM，不支持任何节点视图方法。

作为节点视图返回的对象必须符合此接口。

- `dom: DOMNode`

  表示文档节点的外部 DOM 节点。

- `contentDOM⁠?: HTMLElement`

  应保存节点内容的 DOM 节点。仅当节点视图也定义dom属性并且其节点类型不是叶节点类型时才有意义。当它出现时，ProseMirror 将负责将节点的子节点渲染到其中。当它不存在时，节点视图本身负责渲染（或决定不渲染）其子节点。

-
  `
  update⁠?: fn(
    node: Node,
    decorations: readonly Decoration[],
    innerDecorations: DecorationSource
  ) → boolean
  `

  当给定时，这将在视图自身更新时被调用。它将被给予一个节点（可能是不同类型的），节点周围的活动装饰数组（它们是自动绘制的，如果节点视图对它们不感兴趣，则可能会忽略），以及表示的装饰源应用于节点内容的任何装饰（同样可以被忽略）。如果能够更新到该节点，则应返回 true，否则返回 false。如果节点视图有contentDOM属性（或没有dom属性），更新其子节点将由 ProseMirror 处理。

- `selectNode⁠?: fn()`

  可用于覆盖节点选定状态（作为节点选择）的显示方式。

- `deselectNode⁠?: fn()`

  当定义一个selectNode方法时，你还应该提供一个 deselectNode方法来再次消除效果。

-
  `
  setSelection⁠?: fn(
    anchor: number,
    head: number,
    root: Document | ShadowRoot
  )
  `

  这将被调用来处理设置节点内的选择。anchor和位置head是相对于节点的起点。默认情况下，将在与这些位置对应的 DOM 位置之间创建 DOM 选择，但如果您覆盖它，您可以执行其他操作。

- `stopEvent⁠?: fn(event: Event) → boolean`

  可用于防止编辑器视图尝试处理从节点视图冒出的部分或全部 DOM 事件。编辑器不会处理返回 true 的事件。

- `ignoreMutation⁠?: fn(mutation: MutationRecord) → boolean`

  当视图中发生DOM 突变或选择更改时调用。当更改是选择更改时，记录将具有type属性 "selection"（对于本机突变记录不会发生）。如果编辑器应该重新读取选择或重新解析突变周围的范围，则返回 false，如果可以安全地忽略它，则返回 true。

- `destroy⁠?: fn()`

  当节点视图从编辑器中删除或整个编辑器被销毁时调用。（不适用于标记。）

**interface DOMEventMap extends HTMLElementEventMap**

将事件名称映射到事件对象类型的帮助程序类型，但包括 TypeScript 的 HTMLElementEventMap 不知道的事件。

- `[string]: any`

### Decorations
装饰可以影响文档的绘制方式，而无需实际更改文档。

**class Decoration**
decorations装饰对象可以通过prop提供给视图 。它们有多种变体 - 有关详细信息，请参阅此类的静态成员。

- `from: number`

  装饰的起始位置。

- `to: number`

  结束位置。from与小部件装饰相同。

- `spec: any`

  创建此装饰时提供的规格。如果您在该对象中存储了额外的信息，这会很有用。

-
  `
  static widget(
    pos: number,
    toDOM: fn(view: EditorView, getPos: fn() → number) → DOMNode |
    DOMNode
    ,
    spec⁠?: Object
  ) → Decoration
  `

  创建一个小部件装饰，它是显示在文档中给定位置的 DOM 节点。建议您通过传递一个在视图中实际绘制小部件时调用的函数来延迟渲染小部件，但您也可以直接传递 DOM 节点。getPos可用于查找小部件的当前文档位置。

  - spec
    - `side⁠?: number`

    控制此小部件与文档位置的哪一侧关联。当为负数时，它会在其所在位置的光标之前绘制，并且在该位置插入的内容会在小部件之后结束。当零（默认值）或正数时，小部件在光标之后绘制，并且插入的内容在小部件之前结束。

    当给定位置有多个小部件时，它们的 side值决定它们出现的顺序。那些值较低的首先出现。具有相同值的小部件的顺序side未指定。

    当为marks空时，side还确定了小部件所包装的标记——负数时是前节点的标记，正数时是后节点的标记。

    - `marks⁠?: readonly Mark[]`

    在小部件周围绘制的精确标记集。

    - `stopEvent⁠?: fn(event: Event) → boolean`

    可用于控制哪些 DOM 事件，当它们从该小部件中冒出时，编辑器视图应忽略。

    - `ignoreSelection⁠?: boolean`

    设置后（默认为 false），小部件内的选择更改将被忽略，并且不会导致 ProseMirror 尝试将选择与其选择状态重新同步。

    - `key⁠?: string`

    当比较这种类型的装饰时（为了决定是否需要重绘），ProseMirror 将默认通过身份来比较 widget DOM 节点。如果您传递一个键，则会对该键进行比较，这在您动态生成装饰并且不想存储和重用 DOM 节点时非常有用。确保具有相同密钥的任何小部件都可以互换 - 例如，如果小部件在某些事件处理程序的行为方面有所不同，则它们应该获得不同的密钥。

    - `destroy⁠?: fn(node: DOMNode)`

    当小部件装饰由于映射而被删除时调用

    - `[string]: any`

    规格允许任意附加属性。

-
  `
  static inline(
    from: number,
    to: number,
    attrs: DecorationAttrs,
    spec⁠?: Object
  ) → Decoration
  `

  创建一个内联装饰，它将给定的属性添加到from和之间的每个内联节点to。

    - spec

      - `inclusiveStart⁠?: boolean`

      确定当内容直接插入到该位置时如何映射装饰的左侧 。默认情况下，装饰不会包含新内容，但您可以将其设置为true 包含新内容。

      - `inclusiveEnd⁠?: boolean`

        确定装饰右侧的映射方式。见 inclusiveStart。

      - `[string]: any`

      规格可以具有任意附加属性。

-
  `
  static node(
    from: number,
    to: number,
    attrs: DecorationAttrs,
    spec⁠?: any
  ) → Decoration
  `

  创建节点装饰。from并且to应该精确地指向文档中的节点之前和之后。该节点，并且只有该节点，将接收给定的属性。

**type DecorationAttrs**
添加到修饰节点的一组属性。大多数属性只是直接对应于同名的 DOM 属性，这些属性将被设置为属性的值。以下是例外情况：

- `nodeName⁠?: string`

  当非 null 时，目标节点包装在该类型的 DOM 元素中（并且其他属性应用于该元素）。

- `class⁠?: string`

  要添加到节点已有的类中的CSS 类名或一组以空格分隔的类名 。

- `style⁠?: string`

  要添加到节点现有属性的 CSS 字符串style。

- `[string]: string`

  任何其他属性都被视为常规 DOM 属性。

**class DecorationSet implements DecorationSource**
装饰的集合，其组织方式使得绘图算法可以有效地使用和比较它们。这是一个持久的数据结构——它不会被修改，更新会创建新值。

-
  `
  find(
    start⁠?: number,
    end⁠?: number,
    predicate⁠?: fn(spec: any) → boolean
  ) → Decoration[]
  `

  查找该集合中触及给定范围的所有装饰（包括直接在边界开始或结束的装饰）并在其规范上匹配给定谓词。当 start和end被省略时，将考虑集合中的所有装饰。如果predicate未给出，则假定所有装饰都匹配。

- `map(mapping: Mapping, doc: Node, options⁠?: Object) → DecorationSet`

  映射装饰集以响应文档中的更改。

  - options
    - `onRemove⁠?: fn(decorationSpec: any)`

    当给定时，将为每个由于映射而被删除的装饰调用此函数，并传递该装饰的规范。

    - `add(doc: Node, decorations: Decoration[]) → DecorationSet`

    将给定的装饰数组添加到集合中的装饰中，生成一个新的集合。需要访问当前文档以创建适当的树结构。

    - `remove(decorations: Decoration[]) → DecorationSet`

    创建一个新集合，其中包含该集合中的装饰，减去给定数组中的装饰。

    - `static create(doc: Node, decorations: Decoration[]) → DecorationSet`

    使用给定文档的结构创建一组装饰。

    - `static empty: DecorationSet`

    空的装饰品。

**interface DecorationSource**
可以提供装饰的物体 。由 实现DecorationSet，并传递给节点视图。

- `map(mapping: Mapping, node: Node) → DecorationSource`

  映射装饰集以响应文档中的更改。